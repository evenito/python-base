from time import sleep, time
from typing import Any
from uuid import uuid4

import httpx

from evenito.auth.feature_flag import feature_flag_cache, is_feature_flag_enabled


def get_mocked_client(status_code: int, headers: dict[str, Any], content: bytes):
    # Define the mock response function
    def mock_response(request):
        return httpx.Response(
            status_code=status_code,
            headers=headers,
            content=content,
        )

    # Create the mock transport
    mock_transport = httpx.MockTransport(mock_response)

    # Create the client with the mock transport
    return httpx.Client(transport=mock_transport)


def is_feature_flag_should_return_true():
    client = get_mocked_client(status_code=204, headers={"Cache-Control": "max-age=3600"}, content=b"")
    space_id = uuid4()
    feature_name = "feature_name"
    is_enabled = is_feature_flag_enabled(feature_name, space_id, http_client=client)
    assert is_enabled

    request_cache_key = f"{feature_name}:{space_id}"
    cached_response = feature_flag_cache.response_map[request_cache_key]
    assert cached_response is not None
    assert cached_response.return_value is True
    assert cached_response.ttl > int(time() * 1000)


def is_feature_flag_should_return_false_if_server_return_401():
    client = get_mocked_client(status_code=401, headers={"Cache-Control": "max-age=3600"}, content=b"")
    space_id = uuid4()
    is_enabled = is_feature_flag_enabled("feature_name", space_id, http_client=client)
    assert not is_enabled


def is_feature_flag_should_return_false_if_server_return_403():
    client = get_mocked_client(status_code=403, headers={"Cache-Control": "max-age=3600"}, content=b"")
    space_id = uuid4()
    is_enabled = is_feature_flag_enabled("feature_name", space_id, http_client=client)
    assert not is_enabled


def is_feature_flag_should_return_value_event_without_response_header():
    space_id = uuid4()
    feature_name = "feature_name"
    is_enabled = is_feature_flag_enabled(
        # no cache header
        feature_name,
        space_id,
        http_client=get_mocked_client(status_code=204, headers={}, content=b""))
    assert is_enabled


def is_feature_flag_should_return_cached_response_value():
    space_id = uuid4()
    feature_name = "feature_name"
    is_enabled = is_feature_flag_enabled(
        # cached
        feature_name,
        space_id,
        http_client=get_mocked_client(status_code=403, headers={"Cache-Control": "max-age=2"}, content=b""))
    assert not is_enabled

    is_enabled = is_feature_flag_enabled(
        # not cached
        feature_name,
        space_id,
        http_client=get_mocked_client(status_code=204, headers={"Cache-Control": "max-age=10"}, content=b""))
    assert not is_enabled

    sleep(2)

    is_enabled = is_feature_flag_enabled(
        # cached
        feature_name,
        space_id,
        http_client=get_mocked_client(status_code=204, headers={"Cache-Control": "max-age=3"}, content=b""))
    assert is_enabled


def is_feature_flag_should_not_cache_401():
    client = get_mocked_client(status_code=401, headers={"Cache-Control": "max-age=3600"}, content=b"")
    space_id = uuid4()
    feature_name = "feature_name"
    is_enabled = is_feature_flag_enabled(feature_name, space_id, http_client=client)
    assert not is_enabled

    is_enabled = is_feature_flag_enabled(
        # cached
        feature_name,
        space_id,
        http_client=get_mocked_client(status_code=204, headers={"Cache-Control": "max-age=1"}, content=b""))
    assert is_enabled


def is_feature_flag_should_return_value_without_space_id():
    client = get_mocked_client(status_code=204, headers={"Cache-Control": "max-age=3600"}, content=b"")
    feature_name = "feature_name"
    is_enabled = is_feature_flag_enabled(feature_name, http_client=client)
    assert is_enabled
