import os
import time
from typing import Any
from uuid import uuid4

import httpx
import jwt
import pytest
from fastapi import HTTPException
from starlette.datastructures import Headers
from starlette.requests import Request

from evenito.auth.authorize import Authorization


def create_token(user_id: str, valid=2, internal: bool | None = None, check_mock: dict[str, any] | None = None):
    iat = int(time.mktime(time.localtime()))
    exp = iat + valid
    data = {
        "name": "Super Admin",
        "iss": "https://securetoken.google.com/evenito-v3-dev",
        "auth_time": iat,
        "user_id": user_id,
        "sub": user_id,
        "internal": internal,
        "check": check_mock,
        "exp": exp,
    }
    return jwt.encode(data, os.environ.get("AUTH_JWT_SECRET") or "secret", algorithm="HS256")


class MockHttpClient:
    def __init__(
        self,
        status: int = 200,
        return_value: dict[str, Any] | None = None,
    ):
        self.status = status
        self.return_value = return_value or {}

    async def get(self, *_, **__):
        return httpx.Response(status_code=self.status, json=self.return_value)


async def get_authorized_user_should_return_an_authz_user():
    user_id = uuid4()
    space_id = uuid4()
    authz_url = "http://authz_url"

    authorize = Authorization(authz_url, MockHttpClient(return_value={
        "userId": str(user_id),
        "internal": False,
        "check": {f"read:spaces:{space_id}": True},
    }))

    token = create_token(str(user_id), 1000)
    request = Request(
        {
            "type": "http",
            "path": "/",
            "headers": Headers({
                "Authorization": f"Bearer {token}",
                "evenito-space-id": str(space_id),
            }).raw,
            "http_version": "1.1",
            "method": "GET",
            "scheme": "https",
            "client": ("127.0.0.1", 8080),
            "server": ("app.evenito_service.com", 443),
        },
    )
    user = await authorize.get_authorized_user("read", "spaces")(request)
    assert user is not None

    assert user.user_id == user_id
    assert user.internal is False
    assert user.get_check("read", "spaces", str(space_id))


async def get_authorized_user_should_return_an_internal_authz_user():
    space_id = uuid4()
    token = create_token(None, 1000)

    authorize = Authorization("http://authz_url", MockHttpClient(return_value={
        "internal": True,
        "check": {f"read:spaces:{space_id}": True},
    }))

    request = Request(
        {
            "type": "http",
            "path": "/",
            "headers": Headers({
                "Authorization": f"Bearer {token}",
                "evenito-space-id": str(space_id),
            }).raw,
            "http_version": "1.1",
            "method": "GET",
            "scheme": "https",
            "client": ("127.0.0.1", 8080),
            "server": ("app.evenito_service.com", 443),
        },
    )
    user = await authorize.get_authorized_user("read", "spaces")(request)

    assert user.internal is True
    assert user.get_check("read", "spaces", str(space_id))


async def get_authorized_user_should_return_unathorized_user():
    user_id = uuid4()
    space_id = uuid4()

    token = create_token(str(user_id), 0)

    authorize = Authorization("http://authz_url", MockHttpClient(status=401))
    request = Request(
        {
            "type": "http",
            "path": "/",
            "headers": Headers({
                "Authorization": f"Bearer {token}",
                "evenito-space-id": str(space_id),
            }).raw,
            "http_version": "1.1",
            "method": "GET",
            "scheme": "https",
            "client": ("127.0.0.1", 8080),
            "server": ("app.evenito_service.com", 443),
        },
    )
    with pytest.raises(HTTPException) as e:
        await authorize.get_authorized_user("read", "spaces")(request)

    assert e.value.status_code == 401


async def get_authorized_user_should_return_unauthorized_without_space_id_in_header_or_param():
    user_id = uuid4()
    token = create_token(str(user_id), 1000)

    authorize = Authorization("http://authz_url", MockHttpClient())
    request = Request(
        {
            "type": "http",
            "path": "/",
            "headers": Headers({
                "Authorization": f"Bearer {token}",
                # "evenito-space-id": str(space_id), # this should not work without evenito-space-id
            }).raw,
            "http_version": "1.1",
            "method": "GET",
            "scheme": "https",
            "client": ("127.0.0.1", 8080),
            "server": ("app.evenito_service.com", 443),
        },
    )
    async def request_body():
        return {}
    request.body = request_body

    with pytest.raises(HTTPException) as e:
        await authorize.get_authorized_user("read", "spaces")(request)
    assert e.value.status_code == 403


async def get_authorized_user_should_return_unauthorized_without_entity_id_in_param():
    user_id = uuid4()
    space_id = uuid4()
    event_id = uuid4()

    token = create_token(str(user_id), 1000, internal=False, check_mock={f"read:entity:{event_id}": True})
    authorize = Authorization("http://authz_url", MockHttpClient())
    request = Request(
        {
            "type": "http",
            "path": "/",
            "headers": Headers({
                "Authorization": f"Bearer {token}",
                "evenito-space-id": str(space_id),
            }).raw,
            "http_version": "1.1",
            "method": "GET",
            "scheme": "https",
            "client": ("127.0.0.1", 8080),
            "server": ("app.evenito_service.com", 443),
        },
    )
    async def request_body():
        return {}
    request.body = request_body

    with pytest.raises(HTTPException) as e:
        await authorize.get_authorized_user("read", "spaces")(request)
    assert e.value.status_code == 403


async def get_authorized_user_should_return_authorized_with_entity_id_in_param():
    user_id = uuid4()
    space_id = uuid4()
    event_id = uuid4()

    token = create_token(str(user_id), 1000)

    authorize = Authorization("http://authz_url", MockHttpClient(return_value={
        "userId": str(user_id),
        "internal": False,
        "check": {f"read:entity:{event_id}": True},
    }))
    request = Request(
        {
            "type": "http",
            "path": f"/{event_id}",
            "headers": Headers({
                "Authorization": f"Bearer {token}",
                "evenito-space-id": str(space_id),
            }).raw,
            "http_version": "1.1",
            "method": "GET",
            "scheme": "https",
            "client": ("127.0.0.1", 8080),
            "server": ("app.evenito_service.com", 443),
            "path_params": {"id": str(event_id)},
        },
    )

    user = await authorize.get_authorized_user("read", "entity")(request)
    assert user is not None


async def get_authorized_user_should_return_authorized_with_entity_id_in_param_with_specified_param_key():
    user_id = uuid4()
    space_id = uuid4()
    event_id = uuid4()
    token = create_token(str(user_id), 1000, internal=False, check_mock={f"read:entity:{event_id}": True})

    authorize = Authorization("http://authz_url", MockHttpClient(return_value={
        "userId": str(user_id),
        "internal": False,
        "check": {f"read:entity:{event_id}": True},
    }))
    request = Request(
        {
            "type": "http",
            "path": f"/{event_id}",
            "headers": Headers({
                "Authorization": f"Bearer {token}",
                "evenito-space-id": str(space_id),
            }).raw,
            "http_version": "1.1",
            "method": "GET",
            "scheme": "https",
            "client": ("127.0.0.1", 8080),
            "server": ("app.evenito_service.com", 443),
            "path_params": {"event_id": str(event_id)},
        },
    )

    # specify the param key to be 'event_id'
    user = await authorize.get_authorized_user("read", "entity", "event_id")(request)
    assert user is not None
