import unittest.mock

import pytest

from evenito.auth import get_internal_token


@pytest.fixture
def no_service_account_token_file():
    with unittest.mock.patch("builtins.open") as m:
        m.side_effect = FileNotFoundError
        yield m


@pytest.fixture
def service_account_token_file():
    with unittest.mock.patch("builtins.open", unittest.mock.mock_open(read_data="token")) as m:
        yield m


def get_internal_token_should_return_none_if_internal_token_is_not_available(no_service_account_token_file):
    assert get_internal_token() is None


def get_internal_token_should_return_the_contents_of_k8s_service_account_file(service_account_token_file):
    assert get_internal_token() == "token"
