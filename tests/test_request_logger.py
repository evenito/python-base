import json

from evenito.logging import create_request_logger


class MockRequest:
    def __init__(
        self,
        method="GET",
        url="http://example.com/",
        path_params=None,
        query_params=None,
        headers=None,
        body=None,
        route="/",
    ):
        self.method = method
        self.url = url
        self.headers = headers or {"accept-language": "de-CH"}
        self._body = body or b""
        self.path_params = path_params or []
        self.query_params = query_params or []
        self.scope = {"route": route}

    async def body(self):
        return self._body

    async def json(self):
        return json.loads(self._body) if self._body else None


async def request_logger_should_generate_a_valid_payload(capsys):
    logger = create_request_logger()
    await logger(MockRequest())
    captured = capsys.readouterr()
    record = json.loads(captured.out)
    assert record["url"] == "http://example.com/"
    assert record["method"] == "GET"
    assert record["headers"] == {"accept-language": "de-CH"}
    assert record["query"] == []
    assert record["body_base64"] == ""
    assert record["timestamp"]
    assert record["level"] == "info"
    assert record["hostname"]
    assert record["msg"] == "request"
    assert record["context"] == "request"
    assert record["_loc_"]["file"]
    assert record["_loc_"]["func"]
    assert record["_loc_"]["lineno"]


async def request_logger_should_use_json_body_if_available(capsys):
    logger = create_request_logger()
    await logger(MockRequest(method="POST", headers={"content-type": "application/json"}, body=json.dumps({"foo": 1})))
    captured = capsys.readouterr()
    record = json.loads(captured.out)
    assert record["method"] == "POST"
    assert record["headers"] == {"content-type": "application/json"}
    assert "body_base64" not in record
    assert record["body"] == {"foo": 1}
