from uuid import uuid4

import pytest

from evenito.auth.feature_flag import is_feature_flag_enabled
from evenito.pytest_plugin.feature_flags import FeatureFlagMock


def feature_flag_mock_should_automatically_set_all_feature_flag_requests_to_false(
    feature_flags: FeatureFlagMock,
) -> None:
    space_id = uuid4()
    assert not is_feature_flag_enabled("feature_name", space_id)
    feature_flags.assert_called_once("feature_name", space_id)


def feature_flag_mock_should_allow_enabling_a_flag_per_space(feature_flags: FeatureFlagMock) -> None:
    space_id = uuid4()
    feature_flags.set(flag="feature_name", space_id=space_id, value=True)
    assert is_feature_flag_enabled("feature_name", space_id)


def feature_flag_mock_should_keep_count_of_requests(feature_flags: FeatureFlagMock) -> None:
    space_id = uuid4()
    is_feature_flag_enabled("feature_name", space_id)
    feature_flags.assert_called_once("feature_name", space_id)
    is_feature_flag_enabled("feature_name", space_id)
    feature_flags.assert_called("feature_name", space_id, 2)


def feature_flag_mock_assert_called_once_should_raise_if_not_called(feature_flags: FeatureFlagMock) -> None:
    with pytest.raises(AssertionError):
        feature_flags.assert_called_once("feature_name", uuid4())


def feature_flag_mock_assert_called_once_should_not_raise_if_called_once(feature_flags: FeatureFlagMock) -> None:
    space_id = uuid4()
    is_feature_flag_enabled("feature_name", space_id)
    feature_flags.assert_called_once("feature_name", space_id)


def feature_flag_mock_assert_called_should_raise_if_not_called(feature_flags: FeatureFlagMock) -> None:
    with pytest.raises(AssertionError):
        feature_flags.assert_called("feature_name", uuid4(), 1)


def feature_flag_mock_assert_not_called_should_raise_if_called(feature_flags: FeatureFlagMock) -> None:
    space_id = uuid4()
    is_feature_flag_enabled("feature_name", space_id)
    with pytest.raises(AssertionError):
        feature_flags.assert_not_called("feature_name", space_id)


def feature_flag_mock_assert_not_called_should_not_raise_if_not_called(feature_flags: FeatureFlagMock) -> None:
    feature_flags.assert_not_called("feature_name", uuid4())


def feature_flag_mock_assert_called_once_should_raise_if_called_more_than_once(feature_flags: FeatureFlagMock) -> None:
    space_id = uuid4()
    is_feature_flag_enabled("feature_name", space_id)
    is_feature_flag_enabled("feature_name", space_id)
    with pytest.raises(AssertionError):
        feature_flags.assert_called_once("feature_name", space_id)


def feature_flag_mock_assert_called_should_raise_if_called_more_than_defined(feature_flags: FeatureFlagMock) -> None:
    space_id = uuid4()
    is_feature_flag_enabled("feature_name", space_id)
    is_feature_flag_enabled("feature_name", space_id)
    with pytest.raises(AssertionError):
        feature_flags.assert_called("feature_name", space_id, 1)
