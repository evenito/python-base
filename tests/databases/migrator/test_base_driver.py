import logging
from collections.abc import Awaitable
from inspect import isawaitable
from pathlib import Path

import pytest

from evenito.databases.migrator.base import FailedMigrationError, MigrationLogItem
from evenito.databases.migrator.driver.base_driver import MigrationDriver
from evenito.databases.migrator.migration_files import MigrationFile
from evenito.databases.migrator.utils import checksum


class ShouldNotHaveCalledError(Exception):
    def __init__(self):
        super().__init__("Should not have been called")


class DummyTestDriver(MigrationDriver):
    @property
    def is_async(self) -> bool:
        return False

    def _load_applied_migrations(self) -> list[MigrationLogItem] | Awaitable[list[MigrationLogItem]]:
        if self.is_async:
            async def _load() -> list[MigrationLogItem]:
                return []

            return _load()
        return []

    def _ensure_migrations_history_table(self) -> None | Awaitable[None]:
        if self.is_async:
            async def _ensure() -> None:
                pass
            return _ensure()
        return None

    def _apply_pending_migration(self, migration: MigrationFile) -> None | Awaitable[None]:
        if self.is_async:
            async def _apply() -> None:
                pass
            return _apply()
        return None


@pytest.fixture
def migration_dir(tmp_path: Path) -> Path:
    files = [
        "V1__test.sql",
        "V2__test.sql",
        "R__function.sql",
        "random.sql",
        "README.md",
    ]
    for file in files:
        tmp_path.joinpath(file).write_text(file)
    return tmp_path


def base_driver_should_collect_migrations_as_part_of_initialization(migration_dir: Path) -> None:
    driver = DummyTestDriver(migrations_dir=migration_dir)
    driver.initialize()
    assert len(driver.pending_version_migrations) == 2
    assert len(driver.pending_repeatable_migrations) == 1


def base_driver_should_initialize_from_sync_implementation(migration_dir: Path) -> None:
    class TestDriver(DummyTestDriver):
        @property
        def is_async(self) -> bool:
            return False

    driver = TestDriver(migrations_dir=migration_dir)
    driver.initialize()


async def base_driver_should_initialize_from_async_implementation(migration_dir: Path) -> None:
    class TestDriver(DummyTestDriver):
        @property
        def is_async(self) -> bool:
            return True

        def _load_applied_migrations(self) -> Awaitable[list[MigrationLogItem]]:
            async def _load() -> list[MigrationLogItem]:
                return []

            return _load()

    driver = TestDriver(migrations_dir=migration_dir)
    result = driver.initialize()
    assert isawaitable(result)
    await result


def base_driver_migrate_should_verify_integrity(migration_dir: Path) -> None:
    class TestDriver(DummyTestDriver):
        def _load_applied_migrations(self) -> list[MigrationLogItem]:
            return [
                MigrationLogItem("V1__test.sql", checksum("V1__test.sql"), 0, 0, False),
            ]

    driver = TestDriver(migrations_dir=migration_dir)
    driver.initialize()

    with pytest.raises(FailedMigrationError):
        driver.migrate()


def migrate_should_create_the_migrations_table(migration_dir: Path) -> None:
    class TestDriver(DummyTestDriver):
        def __init__(self, *, migrations_dir: Path) -> None:
            super().__init__(migrations_dir=migrations_dir)
            self._ensured = False

        def ensured(self) -> bool:
            return self._ensured

        def _ensure_migrations_history_table(self) -> None | Awaitable[None]:
            self._ensured = True
            return None

    driver = TestDriver(migrations_dir=migration_dir)
    driver.initialize()
    driver.migrate()

    assert driver.ensured()


async def migrate_should_create_the_migrations_table_with_async_driver(migration_dir: Path) -> None:
    class TestDriver(DummyTestDriver):
        def __init__(self, *, migrations_dir: Path) -> None:
            super().__init__(migrations_dir=migrations_dir)
            self._ensured = False

        @property
        def is_async(self) -> bool:
            return True

        def ensured(self) -> bool:
            return self._ensured

        def _ensure_migrations_history_table(self) -> None | Awaitable[None]:
            async def _ensure() -> None:
                self._ensured = True

            return _ensure()

    driver = TestDriver(migrations_dir=migration_dir)
    fut = driver.initialize()
    assert isawaitable(fut)
    await fut
    fut = driver.migrate()
    assert isawaitable(fut)
    await fut

    assert driver.ensured()


def migrate_should_apply_pending_versioned_migrations(migration_dir: Path) -> None:
    class TestDriver(DummyTestDriver):
        def __init__(self, *, migrations_dir: Path) -> None:
            super().__init__(migrations_dir=migrations_dir)
            self._applied = []

        def applied(self) -> list[MigrationFile]:
            return self._applied

        def _load_applied_migrations(self) -> list[MigrationLogItem]:
            return [
                MigrationLogItem("V1__test.sql", checksum("V1__test.sql"), 0, 0, True),
            ]

        def _apply_pending_migration(self, migration: MigrationFile) -> None | Awaitable[None]:
            self._applied.append(migration)
            return None

    driver = TestDriver(migrations_dir=migration_dir)
    driver.initialize()
    driver.migrate()

    assert driver.applied()


async def migrate_should_apply_pending_versioned_migrations_with_async_driver(migration_dir: Path) -> None:
    applied_migrations = []
    class TestDriver(DummyTestDriver):
        def __init__(self, *, migrations_dir: Path) -> None:
            super().__init__(migrations_dir=migrations_dir)

        @property
        def is_async(self) -> bool:
            return True

        def _load_applied_migrations(self) -> Awaitable[list[MigrationLogItem]]:
            async def _load() -> list[MigrationLogItem]:
                return [
                    MigrationLogItem("V1__test.sql", checksum("V1__test.sql"), 0, 0, True),
                ]

            return _load()

        def _apply_pending_migration(self, migration: MigrationFile) -> None | Awaitable[None]:
            async def _apply() -> None:
                applied_migrations.append(migration)

            return _apply()

    driver = TestDriver(migrations_dir=migration_dir)
    fut = driver.initialize()
    assert isawaitable(fut)
    await fut
    fut = driver.migrate()
    assert isawaitable(fut)
    await fut

    assert applied_migrations


def migrate_should_not_try_to_apply_pending_versioned_migrations_when_there_are_none(migration_dir: Path) -> None:
    applied_migrations = []
    class TestDriver(DummyTestDriver):
        def __init__(self, *, migrations_dir: Path) -> None:
            super().__init__(migrations_dir=migrations_dir)

        def _load_applied_migrations(self) -> list[MigrationLogItem]:
            return [
                MigrationLogItem("V1__test.sql", checksum("V1__test.sql"), 0, 0, True),
                MigrationLogItem("V2__test.sql", checksum("V2__test.sql"), 0, 0, True),
                MigrationLogItem("R__function.sql", checksum("R__function.sql"), 0, 0, True),
            ]

        def _apply_pending_migration(self, migration: MigrationFile) -> None | Awaitable[None]:
            applied_migrations.append(migration)
            return None

    driver = TestDriver(migrations_dir=migration_dir)
    driver.initialize()
    driver.migrate()

    assert not applied_migrations


async def migrate_should_not_try_to_apply_pending_versioned_migrations_with_async_driver_when_there_are_none(
    migration_dir: Path,
) -> None:
    applied_migrations = []
    class TestDriver(DummyTestDriver):
        def __init__(self, *, migrations_dir: Path) -> None:
            super().__init__(migrations_dir=migrations_dir)

        @property
        def is_async(self) -> bool:
            return True

        def _load_applied_migrations(self) -> Awaitable[list[MigrationLogItem]]:
            async def _load() -> list[MigrationLogItem]:
                return [
                    MigrationLogItem("V1__test.sql", checksum("V1__test.sql"), 0, 0, True),
                    MigrationLogItem("V2__test.sql", checksum("V2__test.sql"), 0, 0, True),
                    MigrationLogItem("R__function.sql", checksum("some other data"), 0, 0, True),
                ]

            return _load()

        def _apply_pending_migration(self, migration: MigrationFile) -> None | Awaitable[None]:
            async def _apply() -> None:
                applied_migrations.append(migration)

            return _apply()

    driver = TestDriver(migrations_dir=migration_dir)
    fut = driver.initialize()
    assert isawaitable(fut)
    await fut
    fut = driver.migrate()
    assert isawaitable(fut)
    await fut

    assert applied_migrations == [
        driver.pending_repeatable_migrations[0],
    ]


def migrate_should_apply_updated_repeatable_migrations(migration_dir: Path) -> None:
    applied_migrations = []
    class TestDriver(DummyTestDriver):
        def __init__(self, *, migrations_dir: Path) -> None:
            super().__init__(migrations_dir=migrations_dir)

        def _load_applied_migrations(self) -> list[MigrationLogItem]:
            return [
                MigrationLogItem("V1__test.sql", checksum("V1__test.sql"), 0, 0, True),
                MigrationLogItem("V2__test.sql", checksum("V2__test.sql"), 0, 0, True),
                MigrationLogItem("R__function.sql", checksum("some other data"), 0, 0, True),
            ]

        def _apply_pending_migration(self, migration: MigrationFile) -> None | Awaitable[None]:
            applied_migrations.append(migration)
            return None

    driver = TestDriver(migrations_dir=migration_dir)
    driver.initialize()
    driver.migrate()

    assert applied_migrations == [
        driver.pending_repeatable_migrations[0],
    ]


async def migrate_should_apply_updated_repeatable_migrations_with_async_driver(migration_dir: Path) -> None:
    applied_migrations = []
    class TestDriver(DummyTestDriver):
        def __init__(self, *, migrations_dir: Path) -> None:
            super().__init__(migrations_dir=migrations_dir)

        @property
        def is_async(self) -> bool:
            return True

        def _load_applied_migrations(self) -> Awaitable[list[MigrationLogItem]]:
            async def _load() -> list[MigrationLogItem]:
                return [
                    MigrationLogItem("V1__test.sql", checksum("V1__test.sql"), 0, 0, True),
                    MigrationLogItem("V2__test.sql", checksum("V2__test.sql"), 0, 0, True),
                    MigrationLogItem("R__function.sql", checksum("some other data"), 0, 0, True),
                ]

            return _load()

        def _apply_pending_migration(self, migration: MigrationFile) -> None | Awaitable[None]:
            async def _apply() -> None:
                applied_migrations.append(migration)

            return _apply()

    driver = TestDriver(migrations_dir=migration_dir)
    fut = driver.initialize()
    assert isawaitable(fut)
    await fut
    fut = driver.migrate()
    assert isawaitable(fut)
    await fut

    assert applied_migrations == [
        driver.pending_repeatable_migrations[0],
    ]


def migrate_should_not_try_to_apply_updated_repeatable_migrations_when_there_are_none(migration_dir: Path) -> None:
    class TestDriver(DummyTestDriver):
        def __init__(self, *, migrations_dir: Path) -> None:
            super().__init__(migrations_dir=migrations_dir)

        def _load_applied_migrations(self) -> list[MigrationLogItem]:
            return [
                MigrationLogItem("V1__test.sql", checksum("V1__test.sql"), 0, 0, True),
                MigrationLogItem("V2__test.sql", checksum("V2__test.sql"), 0, 0, True),
                MigrationLogItem("R__function.sql", checksum("R__function.sql"), 0, 0, True),
            ]

        def _apply_pending_migration(self, migration: MigrationFile) -> None | Awaitable[None]:
            raise ShouldNotHaveCalledError

    driver = TestDriver(migrations_dir=migration_dir)
    driver.initialize()
    driver.migrate()


async def migrate_should_not_try_to_apply_updated_repeatable_migrations_with_async_driver_when_there_are_none(
    migration_dir: Path,
) -> None:
    class TestDriver(DummyTestDriver):
        def __init__(self, *, migrations_dir: Path) -> None:
            super().__init__(migrations_dir=migrations_dir)

        @property
        def is_async(self) -> bool:
            return True

        def _load_applied_migrations(self) -> Awaitable[list[MigrationLogItem]]:
            async def _load() -> list[MigrationLogItem]:
                return [
                    MigrationLogItem("V1__test.sql", checksum("V1__test.sql"), 0, 0, True),
                    MigrationLogItem("V2__test.sql", checksum("V2__test.sql"), 0, 0, True),
                    MigrationLogItem("R__function.sql", checksum("R__function.sql"), 0, 0, True),
                ]

            return _load()

        def _apply_pending_migration(self, migration: MigrationFile) -> None | Awaitable[None]:
            async def _apply() -> None:
                raise ShouldNotHaveCalledError

            return _apply()

    driver = TestDriver(migrations_dir=migration_dir)
    fut = driver.initialize()
    assert isawaitable(fut)
    await fut
    fut = driver.migrate()
    assert isawaitable(fut)
    await fut


def migrate_dry_run_should_only_logs_the_pending_migrations(
    migration_dir: Path,
    caplog: pytest.LogCaptureFixture,
) -> None:
    class TestDriver(DummyTestDriver):
        def __init__(self, *, migrations_dir: Path) -> None:
            super().__init__(migrations_dir=migrations_dir)

        def _apply_pending_migration(self, migration: MigrationFile) -> None | Awaitable[None]:
            raise ShouldNotHaveCalledError

    caplog.set_level(logging.INFO)

    driver = TestDriver(migrations_dir=migration_dir)
    driver.initialize()
    driver.migrate(dry_run=True)

    assert len(caplog.records) == 3
    assert caplog.records[0].levelname == "INFO"
    assert "V1__test.sql" in caplog.records[0].message
    assert caplog.records[1].levelname == "INFO"
    assert "V2__test.sql" in caplog.records[1].message
    assert caplog.records[2].levelname == "INFO"
    assert "R__function.sql" in caplog.records[2].message
