from pathlib import Path

from evenito.databases.migrator import migration_files
from evenito.databases.migrator.utils import checksum


def collector_should_find_versioned_migrations(tmp_path: Path):
    files = ["V1__test.sql", "V2__test.sql", "random.sql", "REAMDE.md"]

    for file in files:
        tmp_path.joinpath(file).write_text("")

    collected = migration_files.collect(tmp_path)

    assert [v.filename for v in collected] == [
        tmp_path.joinpath("V1__test.sql"),
        tmp_path.joinpath("V2__test.sql"),
    ]


def migration_files_should_calculate_checksum(tmp_path: Path):
    tmp_path.joinpath("V1__test.sql").write_text("CREATE TABLE foo (id bigint);")
    file = migration_files.MigrationFile.read(tmp_path.joinpath("V1__test.sql"))
    assert file.checksum == checksum("CREATE TABLE foo (id bigint);")


def migration_file_should_populate_fields(tmp_path: Path):
    file = (tmp_path / "V12__file.sql")
    with file.open("w") as f:
        f.write("FOO")

    migration_file = migration_files.MigrationFile.read(file)

    assert migration_file.id == 12
    assert migration_file.script == "V12__file.sql"
    assert migration_file.filename == file
    assert migration_file.contents == "FOO"
    assert migration_file.checksum == checksum("FOO")
    assert migration_file.type == migration_files.MigrationType.Version


def migration_file_should_support_zero_padded_ids(tmp_path: Path):
    file = (tmp_path / "V0012__file.sql")
    with file.open("w") as f:
        f.write("data")

    migration_file = migration_files.MigrationFile.read(file)

    assert migration_file.id == 12
    assert migration_file.script == "V0012__file.sql"


def migration_collector_should_collect_repeatable_migrations(tmp_path: Path):
    files = [
        "R__function.sql",
        "V1__test.sql",
        "V2__test.sql",
    ]

    for file in files:
        tmp_path.joinpath(file).write_text("x")

    collected = migration_files.collect(tmp_path)

    assert [v.filename for v in collected] == [
        tmp_path.joinpath("R__function.sql"),
        tmp_path.joinpath("V1__test.sql"),
        tmp_path.joinpath("V2__test.sql"),
    ]


def repeatable_migrations_should_populate_fields(tmp_path: Path):
    file = (tmp_path / "R__function.sql")
    with file.open("w") as f:
        f.write("FOO")

    migration_file = migration_files.MigrationFile.read(file)

    assert migration_file.id > 1_000_000
    assert migration_file.script == "R__function.sql"
    assert migration_file.filename == file
    assert migration_file.contents == "FOO"
    assert migration_file.checksum == checksum("FOO")
    assert migration_file.type == migration_files.MigrationType.Repeatable
