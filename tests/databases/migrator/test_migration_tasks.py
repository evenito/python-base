from pathlib import Path

import pytest

from evenito.databases.migrator.base import FailedMigrationError, MigrationFileChangedError, MigrationLogItem
from evenito.databases.migrator.migration_files import MigrationFile
from evenito.databases.migrator.migration_tasks import MigrationTasks
from evenito.databases.migrator.utils import checksum


def migration_tasks_should_successfully_verify_empty_state() -> None:
    migration_tasks = MigrationTasks()
    migration_tasks.verify_integrity()


def migration_tasks_should_successfully_verify_unapplied_migrations() -> None:
    files = [
        MigrationFile(Path("migrations/V1__first.sql"), "FOO"),
        MigrationFile(Path("migrations/V1__second.sql"), "BAR"),
    ]
    migration_tasks = MigrationTasks(files)
    migration_tasks.verify_integrity()


def migration_tasks_should_successfully_verify_unapplied_migrations_when_one_is_already_applied() -> None:
    files = [
        MigrationFile(Path("migrations/V1__first.sql"), "FOO"),
        MigrationFile(Path("migrations/V1__second.sql"), "BAR"),
    ]
    logs = [
        MigrationLogItem("V1__first.sql", checksum("FOO"), 0, 0, True),
    ]
    migration_tasks = MigrationTasks(files, logs)
    migration_tasks.verify_integrity()


def migration_tasks_should_successfully_verify_fully_applied_migrations() -> None:
    files = [
        MigrationFile(Path("migrations/V1__first.sql"), "FOO"),
        MigrationFile(Path("migrations/V1__second.sql"), "BAR"),
    ]
    logs = [
        MigrationLogItem("V1__first.sql", checksum("FOO"), 0, 0, True),
        MigrationLogItem("V1__second.sql", checksum("BAR"), 0, 0, True),
    ]
    migration_tasks = MigrationTasks(files, logs)
    migration_tasks.verify_integrity()


def migration_tasks_should_raise_when_an_applied_migration_has_changed() -> None:
    files = [
        MigrationFile(Path("migrations/V1__first.sql"), "FOO"),
    ]
    logs = [
        MigrationLogItem("V1__first.sql", checksum("BAR"), 0, 0, True),
    ]
    migration_tasks = MigrationTasks(files, logs)
    with pytest.raises(MigrationFileChangedError):
        migration_tasks.verify_integrity()


def verify_integrity_should_raise_if_a_migration_has_failed_previously() -> None:
    files = [
        MigrationFile(Path("migrations/V1__first.sql"), "FOO"),
    ]
    logs = [
        MigrationLogItem("V1__first.sql", checksum("FOO"), 0, 0, False),
    ]
    migration_tasks = MigrationTasks(files, logs)
    with pytest.raises(FailedMigrationError):
        migration_tasks.verify_integrity()


def verify_integrity_should_not_raise_if_migration_failed_previously_but_has_changed() -> None:
    files = [
        MigrationFile(Path("migrations/V1__first.sql"), "NEW"),
    ]
    logs = [
        MigrationLogItem("V1__first.sql", checksum("OLD"), 0, 0, False),
    ]
    migration_tasks = MigrationTasks(files, logs)
    migration_tasks.verify_integrity()


def verify_integrity_should_not_raise_when_a_repeatable_migration_has_changed() -> None:
    files = [
        MigrationFile(Path("migrations/R__first.sql"), "FOO"),
        MigrationFile(Path("migrations/R__second.sql"), "NEW BAR"),
    ]
    logs = [
        MigrationLogItem("R__first.sql", checksum("FOO"), 0, 0, True),
        MigrationLogItem("R__second.sql", checksum("BAR"), 0, 0, True),
    ]
    migration_tasks = MigrationTasks(files, logs)
    migration_tasks.verify_integrity()


def unapplied_versioned_migrations_should_be_empty_by_default() -> None:
    migration_tasks = MigrationTasks()
    assert migration_tasks.unapplied_versioned_migrations() == []


def unapplied_versioned_migrations_should_be_empty_if_all_migrations_have_been_applied() -> None:
    files = [
        MigrationFile(Path("migrations/V1__first.sql"), "FOO"),
        MigrationFile(Path("migrations/V1__second.sql"), "BAR"),
    ]
    logs = [
        MigrationLogItem("V1__first.sql", checksum("FOO"), 0, 0, True),
        MigrationLogItem("V1__second.sql", checksum("BAR"), 0, 0, True),
    ]
    migration_tasks = MigrationTasks(files, logs)
    assert migration_tasks.unapplied_versioned_migrations() == []


def unapplied_versioned_migrations_should_contain_unapplied_migrations() -> None:
    files = [
        MigrationFile(Path("migrations/V1__first.sql"), "FOO"),
        MigrationFile(Path("migrations/V1__second.sql"), "BAR"),
    ]
    migration_tasks = MigrationTasks(files)
    assert migration_tasks.unapplied_versioned_migrations() == files


def unapplied_versioned_migrations_should_be_ordered_by_id() -> None:
    files: list[MigrationFile] = [
        MigrationFile(Path("migrations/V10__second.sql"), "BAR"),
        MigrationFile(Path("migrations/V10__third.sql"), "BAR"),
        MigrationFile(Path("migrations/V1__first.sql"), "FOO"),
    ]
    migration_tasks = MigrationTasks(files)
    assert migration_tasks.unapplied_versioned_migrations() == [files[2], files[0], files[1]]


def unapplied_versioned_migrations_should_contain_unapplied_migrations_when_one_is_already_applied() -> None:
    files = [
        MigrationFile(Path("migrations/V1__first.sql"), "FOO"),
        MigrationFile(Path("migrations/V1__second.sql"), "BAR"),
    ]
    logs = [
        MigrationLogItem("V1__first.sql", checksum("FOO"), 0, 0, True),
    ]
    migration_tasks = MigrationTasks(files, logs)
    assert migration_tasks.unapplied_versioned_migrations() == [files[1]]


def unapplied_versioned_migrations_should_contain_unapplied_migrations_when_second_is_already_applied() -> None:
    files = [
        MigrationFile(Path("migrations/V1__first.sql"), "FOO"),
        MigrationFile(Path("migrations/V1__second.sql"), "BAR"),
    ]
    logs = [
        MigrationLogItem("V1__first.sql", checksum("FOO"), 0, 0, False),
        MigrationLogItem("V1__second.sql", checksum("BAR"), 0, 0, True),
    ]
    migration_tasks = MigrationTasks(files, logs)
    assert migration_tasks.unapplied_versioned_migrations() == [files[0]]


def unapplied_versioned_migrations_should_not_contain_repeatable_migrations() -> None:
    files = [
        MigrationFile(Path("migrations/V1__first.sql"), "FOO"),
        MigrationFile(Path("migrations/V1__second.sql"), "BAR"),
        MigrationFile(Path("migrations/R__first.sql"), "FOO"),
    ]
    logs = [
        MigrationLogItem("V1__first.sql", checksum("FOO"), 0, 0, True),
        MigrationLogItem("V1__second.sql", checksum("BAR"), 0, 0, True),
        ]
    migration_tasks = MigrationTasks(files, logs)
    assert migration_tasks.unapplied_versioned_migrations() == []


def unapplied_repeatable_migrations_should_be_empty_by_default() -> None:
    migration_tasks = MigrationTasks()
    assert migration_tasks.unapplied_repeatable_migrations() == []


def unapplied_repeatable_migrations_should_contain_unapplied_migrations() -> None:
    files = [
        MigrationFile(Path("migrations/R__first.sql"), "FOO"),
        MigrationFile(Path("migrations/R__second.sql"), "BAR"),
    ]
    migration_tasks = MigrationTasks(files)
    assert migration_tasks.unapplied_repeatable_migrations() == files


def unapplied_repeatable_migrations_should_be_empty_if_all_repeated_migrations_are_applied() -> None:
    files = [
        MigrationFile(Path("migrations/R__first.sql"), "FOO"),
        MigrationFile(Path("migrations/R__second.sql"), "BAR"),
    ]
    logs = [
        MigrationLogItem("R__first.sql", checksum("FOO"), 0, 0, True),
        MigrationLogItem("R__second.sql", checksum("BAR"), 0, 0, True),
    ]
    migration_tasks = MigrationTasks(files, logs)
    assert migration_tasks.unapplied_repeatable_migrations() == []


def unapplied_repeatable_migrations_should_not_contain_versioned_migrations() -> None:
    files = [
        MigrationFile(Path("migrations/V1__first.sql"), "FOO"),
        MigrationFile(Path("migrations/V2__second.sql"), "BAR"),
    ]
    migration_tasks = MigrationTasks(files)
    assert migration_tasks.unapplied_repeatable_migrations() == []


def unapplied_repeatable_migrations_should_contain_changed_repeatable_migrations() -> None:
    files = [
        MigrationFile(Path("migrations/R__first.sql"), "FOO"),
        MigrationFile(Path("migrations/R__second.sql"), "NEW BAR"),
    ]
    logs = [
        MigrationLogItem("R__first.sql", checksum("FOO"), 0, 0, True),
        MigrationLogItem("R__second.sql", checksum("BAR"), 0, 0, True),
    ]
    migration_tasks = MigrationTasks(files, logs)
    assert migration_tasks.unapplied_repeatable_migrations() == [files[1]]


def unapplied_migrations_should_return_all_unapplied_migrations_in_order() -> None:
    files = [
        MigrationFile(Path("migrations/R__first.sql"), "FOO"),
        MigrationFile(Path("migrations/R__second.sql"), "BAR"),
        MigrationFile(Path("migrations/V1__first.sql"), "FOO"),
        MigrationFile(Path("migrations/V1__second.sql"), "BAR"),
    ]
    migration_tasks = MigrationTasks(files)
    assert migration_tasks.unapplied_migrations() == [
        files[2],
        files[3],
        files[0],
        files[1],
    ]
