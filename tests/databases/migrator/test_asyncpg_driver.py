from pathlib import Path

import asyncpg
import pytest

from evenito.databases.migrator.driver.asyncpg_driver import AsyncpgMigrationDriver
from tests.databases.conftest import AsyncConnection, DbSettings


async def driver_should_create_a_migration_history_table_and_apply_migrations_and_load_applied_migrations(
    test_db: DbSettings,
    asyncpg_connection: AsyncConnection,
    working_migration_dir: Path,
) -> None:
    driver = AsyncpgMigrationDriver(
        dsn=test_db.dsn,
        migrations_dir=working_migration_dir,
        migrations_history_table="migrations_history_table",
    )
    await driver.initialize()
    await driver.migrate()

    applied = await asyncpg_connection.fetch("SELECT script FROM migrations_history_table")

    assert len(applied) == 2

    assert applied[0][0] == "V1__test.sql"
    assert applied[1][0] == "R__function.sql"

    applied_migrations = await driver._load_applied_migrations()  # noqa: SLF001

    assert len(applied_migrations) == 2

    assert applied_migrations[0].script == "V1__test.sql"
    assert applied_migrations[1].script == "R__function.sql"


async def driver_should_raise_and_log_if_a_migration_can_not_be_applied(
    test_db: DbSettings,
    broken_migration_dir: Path,
) -> None:
    driver = AsyncpgMigrationDriver(
        dsn=test_db.dsn,
        migrations_dir=broken_migration_dir,
    )
    await driver.initialize()

    with pytest.raises(asyncpg.exceptions.SyntaxOrAccessError):
        await driver.migrate()

    applied_migrations = await driver._load_applied_migrations()  # noqa: SLF001

    assert len(applied_migrations) == 2

    assert applied_migrations[0].script == "V1__test.sql"
    assert applied_migrations[0].success
    assert applied_migrations[1].script == "V2__test.sql"
    assert not applied_migrations[1].success


async def driver_should_run_in_transaction(
    test_db: DbSettings,
    broken_migration_dir_with_default_settings: Path,
    asyncpg_connection: AsyncConnection,
) -> None:
    driver = AsyncpgMigrationDriver(
        dsn=test_db.dsn,
        migrations_dir=broken_migration_dir_with_default_settings,
    )
    await driver.initialize()

    with pytest.raises(asyncpg.exceptions.ActiveSQLTransactionError):
        await driver.migrate()


async def driver_should_disable_transaction_if_migrations_ask_it(
    test_db: DbSettings,
    migration_dir_without_transaction: Path,
    asyncpg_connection: AsyncConnection,
) -> None:
    driver = AsyncpgMigrationDriver(
        dsn=test_db.dsn,
        migrations_dir=migration_dir_without_transaction,
    )
    await driver.initialize()

    await driver.migrate()

    indices = await asyncpg_connection.fetch("SELECT * FROM pg_indexes WHERE tablename = 'foo'")
    assert len(indices) == 2
