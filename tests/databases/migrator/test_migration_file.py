from pathlib import Path

from evenito.databases.migrator.migration_files import MigrationFile, MigrationFileConfig
from evenito.databases.migrator.utils import checksum


def migration_file_should_read_sql_comment_toml_front_matter(tmp_path: Path) -> None:
    file = (tmp_path / "V1__file.sql")
    with file.open("w") as f:
        f.write("""-- [config]
-- transaction = false
--
CREATE TABLE foo (id int);
""")

    migration_file = MigrationFile.read(file)

    assert migration_file.contents == "CREATE TABLE foo (id int);\n"
    assert migration_file.checksum == checksum("CREATE TABLE foo (id int);\n")
    assert migration_file.config == MigrationFileConfig(
        transaction=False,
    )


def migration_file_should_not_strip_comments(tmp_path: Path) -> None:
    file = (tmp_path / "V1__file.sql")
    with file.open("w") as f:
        f.write("""-- [config]
-- transaction = false
--

CREATE TABLE foo (id int);
-- comment""")

    migration_file = MigrationFile.read(file)

    assert migration_file.contents == "\nCREATE TABLE foo (id int);\n-- comment"


def migration_file_should_not_strip_starting_comments_if_they_are_not_toml(tmp_path: Path) -> None:
    file = (tmp_path / "V1__file.sql")
    with file.open("w") as f:
        f.write("""-- this is a migration
-- that does things
--
CREATE TABLE foo (id int);""")

    migration_file = MigrationFile.read(file)

    assert migration_file.contents == "-- this is a migration\n-- that does things\n--\nCREATE TABLE foo (id int);"


def migration_file_config_should_be_the_default_if_config_front_matter_is_missing(tmp_path: Path) -> None:
    file = (tmp_path / "V1__file.sql")
    with file.open("w") as f:
        f.write("CREATE TABLE foo (id int);")

    migration_file = MigrationFile.read(file)

    assert migration_file.config == MigrationFileConfig()


def migration_file_config_should_be_the_default_if_config_front_matter_is_empty(tmp_path: Path) -> None:
    file = (tmp_path / "V1__file.sql")
    with file.open("w") as f:
        f.write("""-- [config]
--
CREATE TABLE foo (id int);""")

    migration_file = MigrationFile.read(file)

    assert migration_file.config == MigrationFileConfig()


def migration_file_should_not_remove_comments_after_front_matter(tmp_path: Path) -> None:
    file = (tmp_path / "V1__file.sql")
    with file.open("w") as f:
        f.write("""-- [config]
-- transaction = false
--

-- comment
CREATE TABLE foo (id int);""")

    migration_file = MigrationFile.read(file)

    assert migration_file.contents == "\n-- comment\nCREATE TABLE foo (id int);"
