from pathlib import Path

import psycopg
import pytest

from evenito.databases.migrator.driver.psycopg_driver import PsycopgMigrationDriver
from tests.databases.conftest import DbSettings, SyncConnection


def driver_should_create_a_migration_history_table_and_apply_migrations_and_load_applied_migrations(
    test_db: DbSettings,
    psycopg_connection: SyncConnection,
    working_migration_dir: Path,
) -> None:
    driver = PsycopgMigrationDriver(
        dsn=test_db.dsn,
        migrations_dir=working_migration_dir,
        migrations_history_table="migrations_history_table",
    )
    driver.initialize()
    driver.migrate()

    cur = psycopg_connection.cursor()
    applied = cur.execute("SELECT script FROM migrations_history_table").fetchall()

    assert len(applied) == 2

    assert applied[0][0] == "V1__test.sql"
    assert applied[1][0] == "R__function.sql"

    applied_migrations = driver._load_applied_migrations()  # noqa: SLF001

    assert len(applied_migrations) == 2

    assert applied_migrations[0].script == "V1__test.sql"
    assert applied_migrations[1].script == "R__function.sql"


def driver_should_raise_and_log_if_a_migration_can_not_be_applied(
    test_db: DbSettings,
    broken_migration_dir: Path,
) -> None:
    driver = PsycopgMigrationDriver(
        dsn=test_db.dsn,
        migrations_dir=broken_migration_dir,
    )
    driver.initialize()

    with pytest.raises(psycopg.errors.ProgrammingError):
        driver.migrate()

    applied_migrations = driver._load_applied_migrations()  # noqa: SLF001

    assert len(applied_migrations) == 2

    assert applied_migrations[0].script == "V1__test.sql"
    assert applied_migrations[0].success
    assert applied_migrations[1].script == "V2__test.sql"
    assert not applied_migrations[1].success


def driver_should_run_in_transaction(
    test_db: DbSettings,
    broken_migration_dir_with_default_settings: Path,
    psycopg_connection: SyncConnection,
) -> None:
    driver = PsycopgMigrationDriver(
        dsn=test_db.dsn,
        migrations_dir=broken_migration_dir_with_default_settings,
    )
    driver.initialize()

    with pytest.raises(psycopg.errors.ActiveSqlTransaction):
        driver.migrate()

    foo_rows = psycopg_connection.execute("SELECT * FROM foo").fetchall()
    assert len(foo_rows) == 0


def driver_should_disable_transaction_if_migrations_ask_it(
    test_db: DbSettings,
    migration_dir_without_transaction: Path,
    psycopg_connection: SyncConnection,
) -> None:
    driver = PsycopgMigrationDriver(
        dsn=test_db.dsn,
        migrations_dir=migration_dir_without_transaction,
    )
    driver.initialize()

    driver.migrate()

    indices = psycopg_connection.execute("SELECT * FROM pg_indexes WHERE tablename = 'foo'").fetchall()
    assert len(indices) == 2
