from pathlib import Path

import pytest


@pytest.fixture
def working_migration_dir(tmp_path: Path) -> Path:
    files = [
        ("V1__test.sql", "CREATE TABLE foo (id int);"),
        ("R__function.sql", "CREATE FUNCTION foo() RETURNS int AS $$ SELECT 1 $$ LANGUAGE SQL;"),
    ]
    for (file, contents) in files:
        tmp_path.joinpath(file).write_text(contents)
    return tmp_path


@pytest.fixture
def broken_migration_dir(tmp_path: Path) -> Path:
    files = [
        ("V1__test.sql", "CREATE TABLE foo (id int);"),
        ("V2__test.sql", "CREATE TABLE foo (id int);"),
    ]
    for (file, contents) in files:
        tmp_path.joinpath(file).write_text(contents)
    return tmp_path


@pytest.fixture
def broken_migration_dir_with_default_settings(tmp_path: Path) -> Path:
    files = [
        ("V1__test.sql", "CREATE TABLE foo (id int primary key, data text);"),
        ("V2__test.sql", "CREATE INDEX CONCURRENTLY foo_idx ON foo (data);"),
    ]
    for (file, contents) in files:
        tmp_path.joinpath(file).write_text(contents)
    return tmp_path


@pytest.fixture
def migration_dir_without_transaction(tmp_path: Path) -> Path:
    files = [
        ("V1__test.sql", "CREATE TABLE foo (id int primary key, data text);"),
        ("V2__test.sql", """-- [config]
-- transaction = false
CREATE INDEX CONCURRENTLY foo_idx ON foo (data);
"""),
    ]
    for (file, contents) in files:
        tmp_path.joinpath(file).write_text(contents)
    return tmp_path
