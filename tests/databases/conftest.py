from collections.abc import AsyncGenerator, Generator
from dataclasses import dataclass
from os import environ
from typing import TYPE_CHECKING, Any
from uuid import uuid4

import asyncpg
import psycopg
import pytest
from psycopg.rows import TupleRow

if TYPE_CHECKING:
    AsyncConnection = asyncpg.Connection[asyncpg.Record]
else:
    AsyncConnection = asyncpg.Connection

SyncConnection = psycopg.Connection[TupleRow]


@dataclass
class DbSettings:
    user: str
    password: str
    database: str
    host: str
    port: int
    dsn: str


@pytest.fixture
def test_db() -> Generator[DbSettings, Any, None]:
    db_name = f"test_{uuid4().hex}"
    db_host = environ.get("POSTGRES_HOST", "localhost")
    db_port = environ.get("POSTGRES_PORT", 5444)
    with psycopg.connect(
        f"dbname=default_db user=user password=password host={db_host} port={db_port}",
        autocommit=True,
    ) as conn:
        conn.execute(f'CREATE DATABASE "{db_name}"')

        yield DbSettings(
            user="user",
            password="password",  # noqa: S106  it's fine this is a test
            database=db_name,
            host=db_host,
            port=int(db_port),
            dsn=f"postgres://user:password@{db_host}:{db_port}/{db_name}",
        )

        conn.execute(f'DROP DATABASE "{db_name}" WITH (FORCE)')


@pytest.fixture
async def asyncpg_connection(test_db: DbSettings) -> AsyncGenerator[AsyncConnection, Any]:
    conn = await asyncpg.connect(test_db.dsn)
    yield conn
    await conn.close()


@pytest.fixture
def psycopg_connection(test_db: DbSettings)  -> Generator[SyncConnection, Any, None]:
    with psycopg.connect(test_db.dsn, autocommit=True) as conn:
        yield conn
