import argparse
import os
from unittest.mock import Mock, patch
from uuid import uuid4

import pytest

from evenito.cli.argparse.env_default import EnvDefault


@pytest.fixture
def parser() -> argparse.ArgumentParser:
    return argparse.ArgumentParser()


def argparse_env_default_should_read_from_env(parser: argparse.ArgumentParser):
    envvar = "ENV_TEST_" + uuid4().hex
    os.environ[envvar] = "test value here"
    parser.add_argument("--foo", action=EnvDefault, envvar=envvar)
    args = parser.parse_args()
    assert args.foo == "test value here"


def argparse_env_default_should_read_first_from_args(parser: argparse.ArgumentParser):
    envvar = "ENV_TEST_" + uuid4().hex
    os.environ[envvar] = "test value here"
    parser.add_argument("--foo", action=EnvDefault, envvar=envvar)
    args = parser.parse_args(["--foo", "real value"])
    assert args.foo == "real value"


def argparse_env_default_should_use_provided_default(parser: argparse.ArgumentParser):
    envvar = "ENV_TEST_" + uuid4().hex
    os.environ[envvar] = "wrong value"
    parser.add_argument("--foo", action=EnvDefault, envvar=envvar, default="default value")
    args = parser.parse_args()
    assert args.foo == "default value"


@patch("sys.exit")
def argparse_env_default_should_support_required_arguments(exit_mock: Mock, parser: argparse.ArgumentParser):
    envvar = "ENV_TEST_" + uuid4().hex
    parser.add_argument("--foo", action=EnvDefault, envvar=envvar, required=True)
    parser.parse_args()
    assert exit_mock.called


@patch("sys.exit")
def argparse_env_default_should_support_not_required_arguments(exit_mock: Mock, parser: argparse.ArgumentParser):
    envvar = "ENV_TEST_" + uuid4().hex
    parser.add_argument("--foo", action=EnvDefault, envvar=envvar, required=False)
    parser.parse_args()
    assert not exit_mock.called
