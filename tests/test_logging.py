import json
import logging
import logging.config

from evenito.logging import get_logging_config, setup_logger


def logger_should_be_set_up_to_output_json(capsys):
    logger = logging.getLogger("test")
    setup_logger(logger, "DEBUG")

    logger.info("This is a message", extra={"some": "extra"})

    captured = capsys.readouterr()
    record = json.loads(captured.out)

    assert record["msg"] == "This is a message"
    assert record["hostname"]
    assert record["timestamp"]
    assert record["context"] == "test"
    assert record["_loc_"]["file"]
    assert record["_loc_"]["func"] == "logger_should_be_set_up_to_output_json"
    assert record["_loc_"]["lineno"]
    assert record["level"] == "info"
    assert record["some"] == "extra"


def logging_config_should_output_json(capsys):
    logging.config.dictConfig(get_logging_config())
    logger = logging.getLogger("context")

    logger.error("random error", extra={"field": "here"})

    captured = capsys.readouterr()
    record = json.loads(captured.out)

    assert record["msg"] == "random error"
    assert record["hostname"]
    assert record["timestamp"]
    assert record["context"] == "context"
    assert record["_loc_"]["file"]
    assert record["_loc_"]["func"] == "logging_config_should_output_json"
    assert record["_loc_"]["lineno"]
    assert record["level"] == "error"
    assert record["field"] == "here"
