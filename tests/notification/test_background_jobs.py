from typing import Any
from uuid import uuid4

import httpx
import pytest
from fastapi import HTTPException

from evenito.notification.background_jobs import (
    create_notification_background_job,
    update_notification_background_job,
)


class MockHttpClient:
    def __init__(
        self,
        status: int = 200,
        return_value: dict[str, Any] | None = None,
    ):
        self.status = status
        self.return_value = return_value or {}

    async def get(self, *_, **__):
        return httpx.Response(status_code=self.status, json=self.return_value)

    async def post(self, *_, **__):
        return httpx.Response(status_code=self.status, json=self.return_value)

    async def patch(self, *_, **__):
        return httpx.Response(status_code=self.status, json=self.return_value)


async def should_create_notification_background_job():
    user_id = uuid4()
    space_id = uuid4()
    job_id = str(uuid4())

    notification_job = await create_notification_background_job(
        space_id,
        user_id,
        "space-unsubscribed-email-export",
        job_id,
        {"space_id": str(space_id)},
        http_client=MockHttpClient(status=200, return_value={
            "id": job_id,
            "userId": str(user_id),
            "spaceId": str(space_id),
            "jobType": "space-unsubscribed-email-export",
        }))
    assert notification_job is not None


async def should_not_create_notification_background_job_if_notification_service_returns_error():
    user_id = uuid4()
    space_id = uuid4()
    job_id = str(uuid4())

    with pytest.raises(HTTPException) as e:
        await create_notification_background_job(
            space_id,
            user_id,
            "space-unsubscribed-email-export",
            job_id,
            {"space_id": str(space_id)},
            http_client=MockHttpClient(status=400))

    assert e.value.args[0] == 400


async def should_not_create_notification_background_job_with_unscerializable_payload():
    user_id = uuid4()
    space_id = uuid4()
    job_id = str(uuid4())

    with pytest.raises(HTTPException) as e:
        await create_notification_background_job(
            space_id,
            user_id,
            "space-unsubscribed-email-export",
            job_id,
            {"space_id": space_id},  # UUID is not JSON serializable
            http_client=MockHttpClient(status=200, return_value={
                "id": job_id,
                "userId": str(user_id),
                "spaceId": str(space_id),
                "jobType": "space-unsubscribed-email-export",
            }))

    assert e.value.args[0] == 400


async def should_update_notification_background_job():
    user_id = uuid4()
    space_id = uuid4()
    job_id = str(uuid4())

    notification_job = await update_notification_background_job(
        job_id,
        "completed",
        data={"space_id": str(space_id)},
        http_client=MockHttpClient(status=200, return_value={
            "id": job_id,
            "userId": str(user_id),
            "spaceId": str(space_id),
        }))
    assert notification_job is not None


async def should_not_update_notification_background_job_if_notification_service_returns_error():
    space_id = uuid4()
    job_id = str(uuid4())

    with pytest.raises(HTTPException) as e:
        await update_notification_background_job(
            job_id,
            "completed",
            data={"space_id": str(space_id)},
            http_client=MockHttpClient(status=400))

    assert e.value.args[0] == 400


async def should_not_update_notification_background_job_with_unserializable_payload():
    space_id = uuid4()
    job_id = str(uuid4())

    with pytest.raises(HTTPException) as e:
        await update_notification_background_job(
            job_id,
            "completed",
            data={"space_id": space_id},  # UUID is not JSON serializable
            http_client=MockHttpClient(status=200, return_value={
                "id": job_id,
            }))

    assert e.value.args[0] == 400
