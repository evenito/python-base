Evenito Python Base
===================

A collection of utilities shared by all services.

Usage
-----

Install as a dependency:
```
poetry add https://gitlab.com/evenito/python-base.git -E psycopg -E authz
```

### Optional features:

Also see [./pyproject.toml](pyproject.toml) most up to date information.

* `asyncpg`: Async PostgreSQL support using `asyncpg` library
* `psycopg`: PostgreSQL support using `psycopg` library
* `authz`: Authorization support
* `monitoring`: Monitoring utilities
* `uvicorn`: Logging support for uvicorn


Developing
----------

To run tests, make sure you have Docker and docker-compose installed.

```
make test
```
