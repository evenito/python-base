from collections import defaultdict
from collections.abc import Generator
from typing import Any, override
from uuid import UUID

import pytest

import evenito.auth.feature_flag


class FeatureFlagRequestCacheMock(evenito.auth.feature_flag.RequestCache):
    """Mock feature flag cache."""

    def __init__(self, call_tracker: dict[str, int]) -> None:
        self.responses: dict[str, bool] = {}
        self.call_tracker = call_tracker

    @override
    def get_response_value(self, key: str) -> dict[str, Any] | bool | str | None:
        """Get cached value of a request-key."""
        self.call_tracker[key] += 1
        return self.responses.get(key, False)

    @override
    def set_response_to_cache(self, key: str, return_value: dict[str, Any] | bool | str, max_age: int) -> None:
        del key, return_value, max_age
        pytest.fail("Feature flag mechanism is mocked")


class FeatureFlagMock:
    """Feature flag mock tool."""

    def __init__(self) -> None:
        self.call_tracker: dict[str, int] = defaultdict(lambda: 0)
        self.feature_flag_cache = FeatureFlagRequestCacheMock(self.call_tracker)

    def set(self, *, flag: str, space_id: UUID | None, value: bool) -> None:
        """Set a feature flag."""
        key = f"{flag:s}:{space_id!s}"
        self.feature_flag_cache.responses[key] = value

    def assert_called_once(self, flag: str, space_id: UUID | None) -> None:
        """Assert that a feature flag was called once."""
        key = f"{flag:s}:{space_id!s}"
        assert self.call_tracker[key] == 1  # noqa: S101

    def assert_not_called(self, flag: str, space_id: UUID | None) -> None:
        """Assert that a feature flag was not called."""
        key = f"{flag:s}:{space_id!s}"
        assert self.call_tracker[key] == 0  # noqa: S101

    def assert_called(self, flag: str, space_id: UUID | None, times: int) -> None:
        """Assert that a feature flag was called."""
        key = f"{flag:s}:{space_id!s}"
        assert self.call_tracker[key] == times  # noqa: S101


@pytest.fixture
def feature_flags() -> Generator[FeatureFlagMock, None, None]:
    """Set a feature flag."""
    old_cache = evenito.auth.feature_flag.feature_flag_cache
    mock = FeatureFlagMock()
    evenito.auth.feature_flag.feature_flag_cache = mock.feature_flag_cache
    yield mock
    evenito.auth.feature_flag.feature_flag_cache = old_cache
