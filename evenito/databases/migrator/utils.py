"""Helper utils for migrations."""
import hashlib


def checksum(data: str) -> bytes:
    """Compute checksum of a string."""
    return hashlib.sha256(data.encode("utf-8")).digest()
