import re
import tomllib
from enum import StrEnum
from pathlib import Path

from pydantic import BaseModel

from evenito.databases.migrator.utils import checksum


class MigrationType(StrEnum):
    """Migration type."""

    Version = "V"
    Repeatable = "R"


class MigrationFileConfig(BaseModel):
    """Configuration for a migration file."""

    transaction: bool = True


class MigrationFile:
    """A single migration file."""

    @classmethod
    def read(cls, filename: Path) -> "MigrationFile":
        """Read a migration file into a MigrationFile object."""
        with filename.open("r") as f:
            contents = f.read()
        return cls(filename, contents)

    @classmethod
    def _extract_config_and_contents(cls, contents: str) -> tuple[str, MigrationFileConfig]:
        if contents.startswith("-- [config]\n"):
            config_lines = []
            content_lines = []
            config_read = False
            lines = contents.split("\n")

            for line in lines:
                if not config_read and line.startswith("--"):
                    config_lines.append(line)
                else:
                    config_read = True
                    content_lines.append(line)

            config_str = "\n".join([line[2:] for line in config_lines])
            config_parsed = tomllib.loads(config_str)
            config = MigrationFileConfig.model_validate(config_parsed.get("config", {}))
            return ("\n".join(content_lines), config)

        return (contents, MigrationFileConfig())

    def __init__(self, filename: Path, contents: str) -> None:
        id_match = re.findall(r"^V(\d+)__", filename.name)
        (self.contents, self.config) = self._extract_config_and_contents(contents)
        self.filename = filename
        self.script = filename.name
        self.id = int(id_match[0]) if id_match else 1_000_000_000
        self.checksum = checksum(self.contents)
        self.type = MigrationType.Version if id_match else MigrationType.Repeatable


def collect(path: Path) -> list[MigrationFile]:
    """Collect migration files from a directory."""
    files = path.glob("*.sql")
    migrations = [
        MigrationFile.read(file)
        for file in files
        if re.fullmatch(r"^.*/V\d+__.+\.sql$", str(file))
        or re.fullmatch(r"^.*/R__.+\.sql$", str(file))
    ]
    return sorted(migrations, key=lambda v: v.script)
