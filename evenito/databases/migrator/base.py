from typing import Any

from .migration_files import MigrationFile, MigrationType


class MigrationError(Exception):
    """Generic migration exception."""


class MigrationDriverError(MigrationError):
    """Migration driver exception."""


class InvalidTableError(MigrationError):
    """Invalid table name exception."""

    def __init__(self, table_name: str) -> None:
        super().__init__(f"Invalid table name {table_name}")


class FailedMigrationError(MigrationError):
    """Failed migration exception."""

    def __init__(self, script: str, *, has_failed_previously: bool = False) -> None:
        super().__init__(f"Migration {script} failed{' previously, not rerunning' if has_failed_previously else ''}")


class MigrationFileChangedError(MigrationError):
    """Migration file has changed in the file system after applying."""

    def __init__(self, file: MigrationFile, applied: "MigrationLogItem") -> None:
        super().__init__(
            f"Migration {file.script} checksum {file.checksum.hex()} "
            f"does not match applied checksum {applied.checksum.hex()}",
        )


class MigrationLogItem:
    """Migration log."""

    @classmethod
    def from_dict(cls, record: dict[str, Any]) -> "MigrationLogItem":
        """Create a log item entry from a database record."""
        return cls(
            record["script"],
            record["checksum"],
            record["applied_at"],
            record["execution_time_microseconds"],
            record["success"],
        )

    def __init__(
        self,
        script: str,
        checksum: bytes,
        applied_at: int,
        execution_time: float,
        success: bool,  # noqa: FBT001
    ) -> None:
        self.script = script
        self.checksum = checksum
        self.applied_at = applied_at
        self.execution_time_microseconds = execution_time
        self.success = success
        self.type = MigrationType(script[0])
