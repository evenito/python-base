import logging
from abc import ABC, abstractmethod
from collections.abc import Awaitable
from inspect import isawaitable
from pathlib import Path

from evenito.databases.migrator.base import MigrationDriverError, MigrationLogItem
from evenito.databases.migrator.migration_files import MigrationFile, collect
from evenito.databases.migrator.migration_tasks import MigrationTasks


class MigrationDriver(ABC):
    """Base class for migration drivers."""

    def __init__(
        self,
        *,
        migrations_dir: Path,
        migrations_history_table: str = "migrations_history",
    ) -> None:
        self._migrations_dir = migrations_dir
        self._migrations_history_table = migrations_history_table
        self._migrations: list[MigrationFile] = []
        self._tasks: MigrationTasks | None = None
        self._logger = logging.getLogger(__name__)

    @property
    def pending_version_migrations(self) -> list[MigrationFile]:
        """Return a list of pending versioned migrations."""
        if self._tasks is None:
            self._logger.warning("Driver has not been initialized.")
            msg = "Driver has not been initialized."
            raise MigrationDriverError(msg)
        return self._tasks.unapplied_versioned_migrations()

    @property
    def pending_repeatable_migrations(self) -> list[MigrationFile]:
        """Return a list of pending repeatable migrations."""
        if self._tasks is None:
            self._logger.warning("Driver has not been initialized.")
            msg = "Driver has not been initialized."
            raise MigrationDriverError(msg)
        return self._tasks.unapplied_repeatable_migrations()

    def initialize(self) -> None | Awaitable[None]:
        """Initialize the driver."""
        self._migrations = collect(self._migrations_dir)
        if self.is_async:
            async def init() -> None:
                await self._initialize_async()
                self._initialize_tasks()
            return init()

        self._initialize_sync()
        self._initialize_tasks()
        self._logger.debug("Driver initialized")
        return None

    def verify_integrity(self) -> None:
        """Verify the integrity of the migration tasks."""
        if self._tasks is None:
            self._logger.warning("Driver has not been initialized.")
            msg = "Driver has not been initialized."
            raise MigrationDriverError(msg)
        self._tasks.verify_integrity()

    def migrate(self, *, dry_run: bool = False) -> None | Awaitable[None]:
        """Migrate the database."""
        if self._tasks is None:
            self._logger.warning("Driver has not been initialized.")
            msg = "Driver has not been initialized."
            raise MigrationDriverError(msg)

        self._tasks.verify_integrity()

        unapplied_migrations = self._tasks.unapplied_migrations()

        if dry_run:
            for migration in unapplied_migrations:
                self._logger.info("Would apply migration %s", migration.script)
            return None

        results = [
            self._ensure_migrations_history_table(),
            self._apply_pending_migrations(unapplied_migrations) if unapplied_migrations else None,
        ]

        if self.is_async:
            async def migrate() -> None:
                for result in results:
                    if result is not None:
                        await result
            return migrate()

        return None

    @property
    @abstractmethod
    def is_async(self) -> bool:
        """True if the driver is asynchronous."""
        ...

    @abstractmethod
    def _load_applied_migrations(self) -> list[MigrationLogItem] | Awaitable[list[MigrationLogItem]]:
        """Load the applied migrations from the database."""
        ...

    @abstractmethod
    def _ensure_migrations_history_table(self) -> None | Awaitable[None]:
        """Create the migrations history table if it does not exist already."""
        ...

    @abstractmethod
    def _apply_pending_migration(self, migration: MigrationFile) -> None | Awaitable[None]:
        """Apply a pending migration.

        Implementations are expected to write the migration to the database
        and update the migrations history table.
        """
        ...

    def _initialize_tasks(self) -> None:
        tasks = MigrationTasks(self._migrations, self._applied_migrations)
        self._tasks = tasks

    def _initialize_sync(self) -> None:
        applied_migrations = self._load_applied_migrations()
        if isinstance(applied_migrations, list):
            self._applied_migrations = applied_migrations
        else:
            self._logger.warning("Could not load applied migrations.")
            msg = (
                "Could not load applied migrations."
                f" Sync Driver ({type(self)}) must implement _load_applied_migrations."
                f" (Current return value is of type {type(applied_migrations)})"
            )
            raise MigrationDriverError(msg)

    async def _initialize_async(self) -> None:
        applied_migrations = self._load_applied_migrations()
        if isawaitable(applied_migrations):
            self._applied_migrations = await applied_migrations
        else:
            self._logger.warning("Could not load applied migrations.")
            msg = (
                "Could not load applied migrations."
                f" Async Driver ({type(self)}) must implement _load_applied_migrations."
                f" (Current return value is of type {type(applied_migrations)})"
            )
            raise MigrationDriverError(msg)

    def _apply_pending_migrations(self, migrations: list[MigrationFile]) -> None | Awaitable[None]:
        if self.is_async:
            async def _apply() -> None:
                for migration in migrations:
                    self._logger.debug("Applying migration %s", migration.script)
                    fut = self._apply_pending_migration(migration)
                    if fut is not None:
                        await fut

            return _apply()

        for migration in migrations:
            self._logger.debug("Applying migration %s", migration.script)
            self._apply_pending_migration(migration)

        return None
