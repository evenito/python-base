import datetime
import logging
from collections.abc import Awaitable
from pathlib import Path
from typing import TYPE_CHECKING

import asyncpg

from evenito.databases.migrator.base import MigrationLogItem
from evenito.databases.migrator.driver.base_driver import MigrationDriver
from evenito.databases.migrator.migration_files import MigrationFile

if TYPE_CHECKING:
    Connection = asyncpg.Connection[asyncpg.Record]
else:
    Connection = asyncpg.Connection

class AsyncpgMigrationDriver(MigrationDriver):
    """Psycopg migration driver."""

    def __init__(
        self,
        *,
        dsn: str | None = None,
        connection: Connection | None = None,
        migrations_dir: Path,
        migrations_history_table: str = "migrations_history",
    ) -> None:
        super().__init__(migrations_dir=migrations_dir, migrations_history_table=migrations_history_table)
        self._dsn = dsn
        self._connection = connection
        self._logger = logging.getLogger(__name__)

    @property
    def is_async(self) -> bool:
        """True if the driver is asynchronous."""
        return True

    async def close(self) -> None:
        """Close the connection to the database.

        Should be called if the driver is initialized with a connection string instead of a connection.
        """
        if self._connection is not None:
            await self._connection.close()
            self._connection = None

    def _load_applied_migrations(self) -> Awaitable[list[MigrationLogItem]]:
        """Load the applied migrations from the database."""
        async def _load() -> list[MigrationLogItem]:
            conn = await self._get_connection()
            try:
                async with conn.transaction():
                    # Transaction is needed to avoid connection going to a bad state when the table does not exist
                    rows = await conn.fetch(
                        f"SELECT * FROM {self._migrations_history_table} ORDER BY applied_at",  # noqa: S608
                    )
                    return [
                        MigrationLogItem.from_dict(dict(row))
                        for row in rows
                    ]
            except asyncpg.exceptions.UndefinedTableError:
                # This will occur before first migrations have been applied
                return []

        return _load()

    def _ensure_migrations_history_table(self) -> Awaitable[None]:
        """Create the migrations history table if it does not exist already."""
        async def _ensure() -> None:
            conn = await self._get_connection()
            query = f"""CREATE TABLE IF NOT EXISTS {self._migrations_history_table} (
                "script" text not null,
                "checksum" bytea not null,
                "applied_at" timestamp with time zone not null,
                "execution_time_microseconds" int8 not null,
                "success" boolean not null,
                PRIMARY KEY ("script", "applied_at")
            )"""
            await conn.execute(query)
            self._logger.info("Created migrations history table '%s'", self._migrations_history_table)

        return _ensure()

    def _apply_pending_migration(self, migration: MigrationFile) -> Awaitable[None]:
        """Apply a pending migration.

        Implementations are expected to write the migration to the database
        and update the migrations history table.
        """
        async def _migrate() -> None:
            start = datetime.datetime.now(datetime.UTC)
            success = False

            conn = await self._get_connection()

            try:
                if migration.config.transaction:
                    async with conn.transaction():
                        await conn.execute(migration.contents)
                else:
                    await conn.execute(migration.contents)
                success = True
                self._logger.info("Applied %s", migration.script)
            except Exception:
                success = False
                self._logger.exception("Failed to apply %s", migration.script)
                raise
            finally:
                duration = datetime.datetime.now(datetime.UTC) - start

                await conn.execute(
                    f"""INSERT INTO {self._migrations_history_table}
                    ("script", "checksum", "applied_at", "execution_time_microseconds", "success")
                    VALUES ($1, $2, $3, $4, $5)""",
                    migration.script,
                    migration.checksum,
                    datetime.datetime.now(datetime.UTC),
                    duration.microseconds,
                    success,
                )

        return _migrate()

    async def _get_connection(self) -> Connection:
        if self._connection is not None:
            return self._connection
        if self._dsn is None:
            msg = "dsn or connection must be provided"
            raise ValueError(msg)
        conn = await asyncpg.connect(self._dsn)
        self._connection = conn
        return conn
