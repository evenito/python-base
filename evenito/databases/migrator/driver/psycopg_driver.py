import datetime
import logging
from pathlib import Path

import psycopg
from psycopg import Connection, sql
from psycopg.errors import UndefinedTable
from psycopg.rows import dict_row

from evenito.databases.migrator.base import MigrationLogItem
from evenito.databases.migrator.driver.base_driver import MigrationDriver
from evenito.databases.migrator.migration_files import MigrationFile


class PsycopgMigrationDriver(MigrationDriver):
    """Psycopg migration driver."""

    def __init__(
        self,
        *,
        dsn: str | None = None,
        connection: Connection | None = None,
        migrations_dir: Path,
        migrations_history_table: str = "migrations_history",
    ) -> None:
        super().__init__(migrations_dir=migrations_dir, migrations_history_table=migrations_history_table)
        self._dsn = dsn
        self._connection = connection
        self._logger = logging.getLogger(__name__)

    @property
    def is_async(self) -> bool:
        """True if the driver is asynchronous."""
        return False

    def close(self) -> None:
        """Close the connection to the database.

        Should be called if the driver is initialized with a connection string instead of a connection.
        """
        if self._connection is not None:
            self._connection.close()
            self._connection = None

    def _load_applied_migrations(self) -> list[MigrationLogItem]:
        """Load the applied migrations from the database."""
        conn = self._get_connection()
        try:
            with conn.transaction():  # noqa: SIM117
                # Transaction is needed to avoid connection going to a bad state when the table does not exist
                with conn.cursor(row_factory=dict_row) as cur:
                    query = sql.SQL(
                        "SELECT * FROM {} ORDER BY applied_at",
                    ).format(sql.Identifier(self._migrations_history_table))
                    return [
                        MigrationLogItem.from_dict(row)
                        for row in cur.execute(query)
                    ]
        except UndefinedTable:
            # This will occur before first migrations have been applied
            self._logger.debug("Could not load applied migrations. This is probably the first run.")
            return []

    def _ensure_migrations_history_table(self) -> None:
        """Create the migrations history table if it does not exist already."""
        self._logger.debug("Creating migrations history table '%s'", self._migrations_history_table)
        conn = self._get_connection()
        query = sql.SQL("""CREATE TABLE IF NOT EXISTS {} (
            "script" text not null,
            "checksum" bytea not null,
            "applied_at" timestamp with time zone not null,
            "execution_time_microseconds" int8 not null,
            "success" boolean not null,
            PRIMARY KEY ("script", "applied_at")
        )""").format(sql.Identifier(self._migrations_history_table))
        conn.execute(query)
        self._logger.info("Ensured migrations history table '%s'", self._migrations_history_table)

    def _apply_pending_migration(self, migration: MigrationFile) -> None:
        """Apply a pending migration.

        Implementations are expected to write the migration to the database
        and update the migrations history table.
        """
        start = datetime.datetime.now(datetime.UTC)
        success = False

        self._logger.debug("Applying %s", migration.script)
        conn = self._get_connection()
        try:
            if migration.config.transaction:
                with conn.transaction():
                    conn.execute(sql.SQL(migration.contents), prepare=False)
            else:
                conn.execute(sql.SQL(migration.contents), prepare=False)
            success = True
            self._logger.info("Applied %s", migration.script)
        except Exception:
            success = False
            self._logger.exception("Failed to apply %s", migration.script)
            raise
        finally:
            duration = datetime.datetime.now(datetime.UTC) - start

            conn.execute(
                sql.SQL("""INSERT INTO {}
                ("script", "checksum", "applied_at", "execution_time_microseconds", "success")
                VALUES (%s, %s, %s, %s, %s)""").format(sql.Identifier(self._migrations_history_table)),
                [
                    migration.script,
                    migration.checksum,
                    datetime.datetime.now(datetime.UTC),
                    duration.microseconds,
                    success,
                ],
            )

    def _get_connection(self) -> Connection:
        if self._connection is not None:
            return self._connection
        if self._dsn is None:
            msg = "dsn or connection must be provided"
            raise ValueError(msg)
        conn = psycopg.connect(self._dsn, autocommit=True)
        self._connection = conn
        return conn
