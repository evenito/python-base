from evenito.databases.migrator.base import FailedMigrationError, MigrationFileChangedError, MigrationLogItem
from evenito.databases.migrator.migration_files import MigrationFile, MigrationType


class MigrationTasks:
    """A helper class to deduce pending migrations."""

    def __init__(self, files: list[MigrationFile] | None = None, logs: list[MigrationLogItem] | None = None) -> None:
        self._files: list[MigrationFile] = files or []
        self._logs: list[MigrationLogItem] = logs or []

    def add_migration_files(self, migration_files: list[MigrationFile]) -> None:
        """Append more migration files to the list."""
        self._files.extend(migration_files)

    def add_migration_logs(self, migration_logs: list[MigrationLogItem]) -> None:
        """Append more migration logs to the list."""
        self._logs.extend(migration_logs)

    def verify_integrity(self) -> None:
        """Verify the integrity of the migration tasks.

        Will raise MigrationFileChangedError if a migration file has changed.
        """
        applied_map = {a.script: a for a in self._logs if a.type == MigrationType.Version}
        for f in self._files:
            if f.script in applied_map:
                a = applied_map[f.script]
                changed = f.checksum != a.checksum
                if a.success and changed:
                    raise MigrationFileChangedError(f, a)
                if not a.success and not changed:
                    raise FailedMigrationError(f.script, has_failed_previously=True)

    def unapplied_versioned_migrations(self) -> list[MigrationFile]:
        """Return a list of unapplied versioned migrations."""
        applied_map = {a.script: a for a in self._logs if a.success}
        unapplied = [
            f for f in self._files
            if f.type == MigrationType.Version and f.script not in applied_map
        ]
        unapplied.sort(key=lambda m: (m.id, m.script))
        return unapplied

    def unapplied_repeatable_migrations(self) -> list[MigrationFile]:
        """Return a list of unapplied repeatable migrations."""
        applied_map = {a.script: a for a in self._logs if a.success}
        unapplied = [
            f for f in self._files
            if f.type == MigrationType.Repeatable and (
                f.script not in applied_map
                or f.checksum != applied_map[f.script].checksum
            )
        ]
        unapplied.sort(key=lambda m: m.script)
        return unapplied

    def unapplied_migrations(self) -> list[MigrationFile]:
        """Return a list of unapplied migrations."""
        return self.unapplied_versioned_migrations() + self.unapplied_repeatable_migrations()
