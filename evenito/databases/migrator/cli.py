import argparse
import asyncio
import logging
import sys
from os import environ
from pathlib import Path
from time import monotonic, sleep

from evenito.cli.argparse.env_default import EnvDefault
from evenito.databases.migrator.driver import MigrationDriver

PROJECT_PATH = environ.get("CI_PROJECT_PATH", "")


class _ConnectionError(Exception):
    pass


def _is_ci(args: argparse.Namespace) -> bool:
    return args.ci in ["true", "1", "TRUE"]


def _cify_path(args: argparse.Namespace, path: str | Path) -> Path:
    if _is_ci(args) and not Path(path).is_absolute() and not str(path).startswith("/builds/" + PROJECT_PATH):
        return Path("/builds/") / PROJECT_PATH / path
    return Path(path)


def _run_migrator(driver: MigrationDriver, args: argparse.Namespace) -> None:
    path = Path(args.migrations_dir)
    if _is_ci(args):
        start_time = monotonic()
        path = _cify_path(args, path)
        while not path.exists():
            if start_time + args.timeout < monotonic():
                msg = f"Timeout waiting for migrations directory {path}"
                raise TimeoutError(msg)
            logging.warning("Migrations directory %s not found. Retrying in 1 second...", path)
            sleep(1)
        logging.debug(
            "Migrations directory %s found after %.2f seconds",
            path, monotonic() - start_time,
        )

    if driver.is_async:
        logging.debug("Running migrations using async driver...")
        async def migrate_async() -> None:
            init = driver.initialize()
            if init:
                await init
            migrate = driver.migrate(dry_run=args.dry_run)
            if migrate:
                await migrate

        asyncio.run(migrate_async())
    else:
        logging.debug("Running migrations using sync driver...")
        driver.initialize()
        driver.migrate(dry_run=args.dry_run)


def _get_driver(args: argparse.Namespace) -> MigrationDriver:
    if not args.dsn:
        msg = "Database connection string is required"
        raise ValueError(msg)

    if not args.migrations_dir:
        msg = "Migrations directory is required"
        raise ValueError(msg)

    migrations_dir = _cify_path(args, args.migrations_dir)

    migrations_history_table = args.table_name or "migrations_history"

    logging.debug(
        "Initializing driver with migrations_dir=%s, migrations_history_table=%s, dsn=%s",
        migrations_dir,
        migrations_history_table,
        args.dsn,
    )
    try:
        import psycopg

        from evenito.databases.migrator.driver.psycopg_driver import PsycopgMigrationDriver

        try:
            return PsycopgMigrationDriver(
                dsn=args.dsn,
                migrations_dir=migrations_dir,
                migrations_history_table=migrations_history_table,
            )
        except psycopg.OperationalError as e:
            raise _ConnectionError from e
    except ImportError:
        pass

    try:
        from evenito.databases.migrator.driver.asyncpg_driver import AsyncpgMigrationDriver

        return AsyncpgMigrationDriver(
            dsn=args.dsn,
            migrations_dir=migrations_dir,
            migrations_history_table=migrations_history_table,
        )
    except ImportError:
        pass

    msg = "psycopg or asyncpg must be installed"
    raise ImportError(msg)


def _build_argparser() -> argparse.ArgumentParser:
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--migrations-dir",
        action=EnvDefault,
        envvar="MIGRATIONS_DIR",
        default="migrations",
        help="Migrations directory",
    )
    parser.add_argument(
        "dsn",
        action=EnvDefault,
        envvar="DATABASE_URL",
        help="Database connection string",
    )
    parser.add_argument(
        "--table-name",
        action=EnvDefault,
        envvar="MIGRATIONS_TABLE_NAME",
        default="migrations_history",
        help="Migration table name",
    )
    parser.add_argument("--verbose", "-v", action="store_true", help="Enable verbose logging")
    parser.add_argument("--dry-run", action="store_true", help="Dry run mode")
    parser.add_argument("--timeout", type=float, default=300.0, help="Timeout in seconds")
    parser.add_argument("--output", "-o", help="Log output file. Default is standard output")
    parser.add_argument(
        "--ci",
        action=EnvDefault,
        envvar="CI",
        default="",
        required=False,
        help="CI environment variable. Set to 'true' to enable CI mode. This makes the migrator wait for a database to"
        " become available and wait for migration files to appear to the specified path.",
    )
    parser.add_argument(
        "--termination-flag-file",
        action=EnvDefault,
        envvar="TERMINATION_FLAG_FILE",
        default="",
        required=False,
        help="Termination flag file. If set, the migrator will create this file when it is terminated.",
    )

    return parser


def _check_for_ci(args: argparse.Namespace) -> None:
    start_time = monotonic()
    migrations_dir = Path(args.migrations_dir)
    if _is_ci(args):
        logging.debug("CI-MODE: Waiting for project directory and migrations directory...")
        start_time = monotonic()

        project_path = Path("/builds/") / PROJECT_PATH
        while not project_path.exists():
            if start_time + args.timeout < monotonic():
                msg = f"Timeout waiting for project directory {project_path}"
                raise TimeoutError(msg)
            sleep(1)

        logging.debug(
            "Project directory %s found after %.2f seconds",
            project_path, monotonic() - start_time,
        )

        migrations_wait = monotonic()

        path = _cify_path(args, migrations_dir)
        while not path.exists():
            if start_time + args.timeout < monotonic():
                msg = f"Timeout waiting for migrations directory {path}"
                raise TimeoutError(msg)
            logging.warning("Migrations directory %s not found. Retrying in 1 second...", path)
            sleep(1)
        logging.debug(
            "Migrations directory %s found after %.2f seconds",
            path, monotonic() - migrations_wait,
        )


def _run(args: argparse.Namespace) -> int:
    level = logging.DEBUG if args.verbose else logging.INFO
    logging.basicConfig(level=level)
    logging.getLogger().name = "migrator"
    _check_for_ci(args)

    if args.output:
        filename = _cify_path(args, args.output)
        logging.basicConfig(level=level, filename=str(filename))

    driver = None
    start_time = monotonic()
    while start_time + args.timeout > monotonic():
        try:
            logging.debug("Trying to connect to the database...")
            driver = _get_driver(args)
            break
        except _ConnectionError:
            logging.warning("Failed to connect to the database. Retrying in 1 second...")
            sleep(1)

    if driver:
        _run_migrator(driver, args)
        logging.info("Done in %.2f seconds", monotonic() - start_time)
    else:
        logging.error("Failed to connect to the database")
        return 1

    return 0


def main() -> int:
    """Run the migrator."""
    parser = _build_argparser()
    args = parser.parse_args()

    result = 127
    try:
        result = _run(args)
        logging.debug("Migrator finished with exit code %d", result)
    except Exception:
        logging.exception("Failed to run the migrator")

    if args.termination_flag_file:
        Path(args.termination_flag_file).touch()
        logging.debug("Created termination flag file %s", args.termination_flag_file)
    sys.exit(result)


if __name__ == "__main__":
    main()
