"""Tools for creating and publishing metrics to GCP."""
import asyncio
import logging
import platform
import threading
import time
from collections.abc import Awaitable, Callable

from google.auth.exceptions import DefaultCredentialsError
from google.cloud.monitoring_v3 import (
    MetricServiceClient,
    Point,
    TimeInterval,
    TimeSeries,
)

logger = logging.getLogger(__name__)


def create_metric_publisher_asyncio(
    project_id: str,
    metric_id: str,
    get_value_fn: Callable[[], dict[str, int] | Awaitable[dict[str, int]]],
) -> asyncio.Task[None] | None:
    """Create and run a metric publisher using asyncio.

    The function expects to find GCP credentials using the environment. If it doesn't find them, it does not run.

    Parameters
    ----------
    project_id:   the project id that stores the metric.
    metric_id:    the metric id.
    get_value_fn: function to get the value to record. Can be sync or async. It should return a new value for
                  each call. The return value must always be a dict of protobuf well known field with one key
                  and a matching value. For example `{'int64_value': 123}`.

    Returns
    -------
    An asyncio task that will publish the metrics.
    """
    import inspect

    try:
        client = MetricServiceClient()
    except DefaultCredentialsError:
        logger.warning("Unable to detect google cloud credentials, not starting the metrics exporter")
        return None

    project_name = f"projects/{project_id}"

    async def report() -> None:
        while True:
            try:
                value = get_value_fn()
                if inspect.isawaitable(value):
                    metric_value = await value
                elif isinstance(value, dict):
                    metric_value = value
                else:
                    logger.warning("Error while exporting a metric %s", value, extra={"error": value})
                    metric_value = None

                if metric_value:
                    series = _create_series_object(metric_id, metric_value, {"project_id": project_id})
                    client.create_time_series(name=project_name, time_series=[series])
            except Exception as e:  # noqa: BLE001 # we want to continue on any error
                logger.warning("Error while exporting a metric %s", e, extra={"error": e})

            await asyncio.sleep(60)

    if project_id and metric_id:
        return asyncio.create_task(report())
    logger.warning("Not starting online users count exporter because of missing configuration")
    return None


def create_metric_publisher_thread(
    project_id: str,
    metric_id: str,
    shutdown_event: threading.Event,
    get_value_fn: Callable[[], dict[str, int]],
) -> threading.Thread | None:
    """Create and run a metric publisher using threading.

    The function expects to find GCP credentials using the environment. If it doesn't find them, it does not run.

    Parameters
    ----------
    project_id:     the project id that stores the metric.
    metric_id:      the metric id.
    shutdown_event: an event object that can be used to signal the thread to stop.
    get_value_fn:   function to get the value to record. It should provide a new value on every call.
                    The return value must always be a dict of protobuf well known field with one key and a
                    matching value. For example `{'int64_value': 123}`.

    Returns
    -------
    A thread object that will publish the metrics.
    """
    def thread_fn() -> None:
        if not project_id or not metric_id:
            logger.warning("Documents processed metric not configured")
            return

        project_name = f"projects/{project_id}"

        try:
            client = MetricServiceClient()
        except DefaultCredentialsError:
            logger.warning("Unable to detect google cloud credentials, not starting the metrics exporter")
            return

        while not shutdown_event.is_set():
            value = get_value_fn()
            series = _create_series_object(metric_id, value, {"project_id": project_id})
            client.create_time_series(name=project_name, time_series=[series])
            shutdown_event.wait(60)

    thread = threading.Thread(target=thread_fn, daemon=True)
    thread.start()
    return thread


def _create_series_object(metric_type: str, value: dict[str, int], labels: dict[str, str] | None = None) -> TimeSeries:
    series = TimeSeries()
    series.metric.type = metric_type
    series.resource.type = "global"
    series.metric.labels["instance"] = platform.node()
    if labels:
        for k, v in labels.items():
            series.resource.labels[k] = v
    now = time.time()
    seconds = int(now)
    nanos = int((now - seconds) * 10 ** 9)
    interval = TimeInterval(
        {"end_time": {"seconds": seconds, "nanos": nanos}},
    )
    point = Point({"interval": interval, "value": value})
    series.points = [point]

    return series
