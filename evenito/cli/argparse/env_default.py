import argparse
import os
from collections.abc import Sequence
from typing import Any


class EnvDefault(argparse.Action):
    """Argument parser action to extract a default value from an environment variable."""

    def __init__(
        self,
        envvar: str,
        *,
        required: bool = True,
        default: str | None = None,
        **kwargs: Any,  # noqa: ANN401
    ) -> None:
        if not default and envvar and envvar in os.environ:
            default = os.environ[envvar]
        if required and default:
            required = False
        super().__init__(default=default, required=required, **kwargs)

    def __call__(
        self,
        parser: argparse.ArgumentParser,  # noqa: ARG002
        namespace: argparse.Namespace,
        values: str | Sequence[Any] | None,
        option_string: str | None = None,  # noqa: ARG002
    ) -> None:
        """Execute the action. Called by argparse."""
        setattr(namespace, self.dest, values)
