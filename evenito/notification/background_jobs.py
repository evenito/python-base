import json
import logging
import os
from typing import Any
from uuid import UUID

from fastapi import HTTPException
from fastapi import status as fastapi_status
from httpx import AsyncClient
from httpx import Response as HttpxResponse
from requests import Response, Session

from evenito.auth.internal_token import get_internal_token

BASE_URL = os.environ.get("NOTIFICATION_SERVICE_URL", "http://notification-service")
INTERNAL_JWT = get_internal_token()

logger = logging.getLogger(__name__)


def check_payload_json_validity(payload: dict[str, Any]) -> None:
    """Check if the payload is valid JSON."""
    try:
        json.dumps(payload)
    except Exception as error:
        raise HTTPException(400, detail=error) from error


def validate_response(response: HttpxResponse | Response) -> None:
    """Validate the http response."""
    if not response or (response.status_code != fastapi_status.HTTP_200_OK):
        raise HTTPException(400, detail=response.text)


async def create_notification_background_job(  # noqa: PLR0913
    space_id: UUID,
    user_id: UUID,
    job_type: str,
    nid: str,
    data: dict[str, Any] | None = None,
    http_client: AsyncClient | None = None,
    base_url: str = BASE_URL,
) -> dict[str, Any]:
    """Create a notification background job."""
    http_client = http_client or AsyncClient()
    result = {}
    payload = {
        "spaceId": str(space_id),
        "userId": str(user_id),
        "jobType": job_type,
        "id": nid,
        "data": data or {},
    }
    check_payload_json_validity(payload)

    try:
        response = await http_client.post(
            f"{base_url}/background-jobs",
            json=payload,
            headers={"Authorization": f"Bearer {INTERNAL_JWT}"},
        )

        validate_response(response)

        result = dict(response.json())
        logger.info("NOTIFICATION_BACKGROUND_JOB CREATED", extra={"result": result})
    except Exception as error:
        logger.warning(
            "NOTIFICATION_BACKGROUND_JOB FAILED TO CREATE",
            extra={
                "error": error,
                "spaceId": space_id,
                "userId": user_id,
                "jobType": job_type,
                "jobId": nid,
                "data": data,
            },
        )
        raise

    return result


def create_notification_background_job_sync(  # noqa: PLR0913
    space_id: UUID,
    user_id: UUID,
    job_type: str,
    nid: str,
    data: dict[str, Any] | None = None,
    http_client: Session | None = None,
    base_url: str = BASE_URL,
) -> dict[str, Any]:
    """Create a notification background job - Sync."""
    http_client = http_client or Session()
    result = {}
    payload = {
        "spaceId": str(space_id),
        "userId": str(user_id),
        "jobType": job_type,
        "id": nid,
        "data": data or {},
    }
    check_payload_json_validity(payload)

    try:
        response = http_client.post(
            f"{base_url}/background-jobs",
            json=payload,
            headers={"Authorization": f"Bearer {INTERNAL_JWT}"},
        )

        validate_response(response)

        result = dict(response.json())
        logger.info("NOTIFICATION_BACKGROUND_JOB CREATED", extra={"result": result})
    except Exception as error:
        logger.warning(
            "NOTIFICATION_BACKGROUND_JOB FAILED TO CREATE",
            extra={
                "error": error,
                "spaceId": space_id,
                "userId": user_id,
                "jobType": job_type,
                "jobId": nid,
                "data": data,
            },
        )
        raise

    return result


async def update_notification_background_job(
    nid: str,
    status: str,
    data: dict[str, Any] | None = None,
    http_client: AsyncClient | None = None,
    base_url: str = BASE_URL,
) -> dict[str, Any]:
    """Update a notification background job."""
    http_client = http_client or AsyncClient()
    payload = {"status": status, "data": data or {}}
    result = {}
    check_payload_json_validity(payload)

    try:
        # update background_job in notification-service
        response = await http_client.patch(
            f"{base_url}/background-jobs/{nid}",
            json=payload,
            headers={"Authorization": f"Bearer {INTERNAL_JWT}"},
        )

        validate_response(response)

        result = dict(response.json())
        logger.info("NOTIFICATION_BACKGROUND_JOB UPDATED", extra={"result": result})
    except Exception as error:
        logger.warning(
            "NOTIFICATION_BACKGROUND_JOB FAILED TO UPDATE",
            extra={"error": error, "status": status, "data": data})
        raise

    return result


def update_notification_background_job_sync(
    nid: str,
    status: str,
    data: dict[str, Any] | None = None,
    http_client: Session | None = None,
    base_url: str = BASE_URL,
) -> dict[str, Any]:
    """Update a notification background job - Sync."""
    http_client = http_client or Session()
    payload = {"status": status, "data": data or {}}
    result = {}
    check_payload_json_validity(payload)

    try:
        # update background_job in notification-service
        response = http_client.patch(
            f"{base_url}/background-jobs/{nid}",
            json=payload,
            headers={"Authorization": f"Bearer {INTERNAL_JWT}"},
        )

        validate_response(response)

        result = dict(response.json())
        logger.info("NOTIFICATION_BACKGROUND_JOB UPDATED", extra={"result": result})
    except Exception as error:
        logger.warning(
            "NOTIFICATION_BACKGROUND_JOB FAILED TO UPDATE",
            extra={"error": error, "status": status, "data": data})
        raise

    return result
