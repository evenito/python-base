def get_internal_token() -> str | None:
    """Get a internal token from the kubernetes secrets."""
    try:
        with open("/var/run/secrets/kubernetes.io/serviceaccount/token") as f:  # noqa: PTH123
            return f.read()
    except FileNotFoundError:
        return None
