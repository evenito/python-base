import os
import re
from time import time
from typing import Any
from uuid import UUID

import httpx
from fastapi import status
from pydantic import BaseModel

from evenito.auth.internal_token import get_internal_token

INTERNAL_JWT = get_internal_token()
AUTHZ_SERVICE_URL = os.environ.get("AUTHZ_SERVICE_URL", "http://platform-authz.bo")
DEFAULT_UUID = UUID(int=0)

class CachedResponse(BaseModel):
    """class object to hold response data."""

    ttl: int
    return_value: dict[str, Any] | bool | str


class RequestCache:
    """Class that caches requests."""

    def __init__(self) -> None:
        self.response_map: dict[str, CachedResponse] = {}

    def set_response_to_cache(self, key: str, return_value: dict[str, Any] | bool | str, max_age: int) -> None:
        """Cache value of a request-key."""
        self.response_map[key] = CachedResponse(
            return_value=return_value,
            ttl=int((time() + max_age) * 1000),
        )

    def get_response_value(self, key: str) -> dict[str, Any] | bool | str | None:
        """Get cached value of a request-key."""
        res_obj = self.response_map.get(key)
        return res_obj.return_value if res_obj and res_obj.ttl > int(time() * 1000) else None


feature_flag_cache = RequestCache()


def is_feature_flag_enabled(
    feature_name: str,
    space_id: UUID | None = DEFAULT_UUID,
    *,
    http_client: httpx.Client | None = None,
) -> bool:
    """Check if feature is enabled for the space_id in Authz."""
    url = f"{AUTHZ_SERVICE_URL}/feature-flags/{feature_name}"
    request_cache_key = f"{feature_name}:{space_id!s}"

    cached_value = feature_flag_cache.get_response_value(request_cache_key)
    if cached_value is not None:
        return bool(cached_value)

    response = (http_client or httpx.Client()).get(
        url,
        headers={
            "Authorization": f"Bearer {INTERNAL_JWT}",
            "evenito-space-id": str(space_id),
        },
    )

    is_enabled = (
        response.status_code >= status.HTTP_200_OK) and (response.status_code < status.HTTP_300_MULTIPLE_CHOICES)
    cache_control = response.headers.get("Cache-Control", "")

    if cache_control and response.status_code != status.HTTP_401_UNAUTHORIZED:
        match = re.search(r"max-age=(\d+)", cache_control)
        max_age = int(match.group(1)) if match else 0

        if max_age > 0:
            feature_flag_cache.set_response_to_cache(
                request_cache_key,
                is_enabled,
                max_age=max_age,
            )


    return is_enabled
