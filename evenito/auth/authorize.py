import json
import logging
import urllib.parse
from collections.abc import Callable, Coroutine
from typing import Any
from uuid import UUID

from fastapi import HTTPException, Request, status
from httpx import AsyncClient
from pydantic import BaseModel, ConfigDict
from pydantic.alias_generators import to_camel

logger = logging.getLogger(__name__)


class AuthzUser(BaseModel):
    """Authz user model."""

    model_config = ConfigDict(alias_generator=to_camel, populate_by_name=True, from_attributes=True)

    user_id: UUID | None = None
    internal: bool | None = False
    email: str | None = None
    type: str | None = None
    teams: list[str] | None = None
    expand: dict[str, list[str]] | None = None
    check: dict[str, bool] | None = None

    def get_expands(self, action: str, entity: str, entity_id: str | None = None) -> list[str] | None:
        """Get expands for an action and entity."""
        key = f"{action}:{entity}"
        if entity_id:
            key += f":{entity_id}"
        return self.expand.get(key, None) if self.expand else None

    def get_check(self, action: str, entity: str, object_id: str) -> bool:
        """Check if a user has access to an object."""
        key = f"{action}:{entity}:{object_id}"
        return self.check.get(key, False) if self.check else False


class AuthzCheck(BaseModel):
    """Authz Check model."""

    action: str
    entity: str
    entity_id: str

    def to_arg(self) -> str:
        """Convert AuthzCheck to string."""
        return f"{self.action}:{self.entity}:{self.entity_id}"


async def authorize_token_from_authz(
    http_client: AsyncClient,
    authz_service_url: str,
    token: str,
    checks: list[AuthzCheck] | None = None,
) -> AuthzUser:
    """Authorize a token from the authz service."""
    response = None

    if token:
        params: dict[str, Any] = {}
        if checks and len(checks):
            params["check"] = [check.to_arg() for check in checks]
        query = urllib.parse.urlencode(params, doseq=True)
        authz_url = f"{authz_service_url}/authenticate?{query}"
        response = await http_client.get(authz_url, headers={"Authorization": f"Bearer {token}"})

    if not response or (response.status_code != status.HTTP_200_OK):
        raise HTTPException(401, "Unauthorized")

    response_dict = dict(response.json())
    try:
        mod_user_json = json.dumps({
            **response_dict,
            "user_id": response_dict.get("user_id"),
            "userId": response_dict.get("userId"),
        })
        return AuthzUser.model_validate_json(mod_user_json)
    except Exception as e:
        logger.exception("Failed to authorize user", extra={"response": response_dict})
        raise HTTPException(status.HTTP_401_UNAUTHORIZED, "Unauthorized") from e


class Authorization:
    """Authorization helper class."""

    authz_service_url = ""

    def __init__(self, authz_service_url: str, http_client: AsyncClient | None = None) -> None:
        self.authz_service_url = authz_service_url
        self.http_client = http_client or AsyncClient()


    async def authenticate_user(self, request: Request, checks: list[AuthzCheck] | None = None) -> AuthzUser:
        """Authenticate a user and return an AuthzUser object.

        # Use as Dependency in FastAPI route
        authorization = Authorization("http://authz_url")
        ...
        @router.get("/some-path")
        async def do_something(user: AuthzUser = Depends(authorization.authenticate_user)) -> Type:
            ...
        """
        token: str = request.headers.get("authorization", " ").split(" ")[1]
        return await authorize_token_from_authz(self.http_client, self.authz_service_url, token, checks)


    def get_authorized_user(
        self,
        action: str,
        entity: str,
        entity_id_param_key: str | None = None,
    ) -> Callable[[Request], Coroutine[Any, Any, AuthzUser]]:
        """Get an authorized user from fastapi request.

        # Use as Dependency in FastAPI route
        authorization = Authorization("http://authz_url")
        ...
        @router.get("/some-path")
        async def do_something(
            user: AuthzUser = Depends(authorization.get_authorized_user("read", "jobs", "job_id")),
        ) -> Type:
            ...
        """

        async def _authorized_user(request: Request) -> AuthzUser:
            entity_id = ""
            if (entity == "spaces" and not (entity_id_param_key and request.path_params.get(entity_id_param_key, ""))):
                # get param from 'evenito-space-id' header, if entity is 'spaces'
                entity_id = request.headers.get("evenito-space-id", "")
            else:
                # get param from specified path param key or default to 'id'
                entity_id = request.path_params.get(entity_id_param_key if entity_id_param_key else "id", "")

            if not entity_id:
                raise HTTPException(status.HTTP_403_FORBIDDEN, "Forbidden")

            user = await self.authenticate_user(request, checks=[AuthzCheck(
                entity=entity,
                action=action,
                entity_id=entity_id,
            )])

            if not user.get_check(action, entity, entity_id):
                raise HTTPException(status.HTTP_403_FORBIDDEN, "Forbidden")
            return user
        return _authorized_user
