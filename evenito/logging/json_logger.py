import logging
import platform
from time import time
from typing import Any

from pythonjsonlogger import jsonlogger


class PinoJsonFormatter(jsonlogger.JsonFormatter):
    """Log formatter that follows the Pino format."""

    def add_fields(self, log_record: dict[str, Any], record: logging.LogRecord, message_dict: dict[str, Any]) -> None:
        """Add fields to the log record."""
        super().add_fields(log_record, record, message_dict)

        if not log_record.get("timestamp"):
            log_record["timestamp"] = int(time() * 1000)
        if log_record.get("level") and isinstance(log_record.get("level"), str):
            log_record["level"] = log_record["level"].lower()
        else:
            log_record["level"] = record.levelname.lower()
        log_record["hostname"] = platform.node()

        log_record["msg"] = log_record["message"]
        del log_record["message"]

        log_record["context"] = record.name
        log_record["_loc_"] = {
            "file": record.pathname,
            "func": record.funcName,
            "lineno": record.lineno,
        }


class UvicornPinoJsonFormatter(PinoJsonFormatter):
    """Log formatter that follows the Pino format for uvicorn."""

    def __init__(self, *args: list[Any], **kwargs: dict[str, Any]) -> None:
        kwargs.pop("use_colors", None)
        super().__init__(args, kwargs)  # type: ignore[no-untyped-call]

    def add_fields(self, log_record: dict[str, Any], record: logging.LogRecord, message_dict: dict[str, Any]) -> None:
        """Add fields to the log record."""
        super().add_fields(log_record, record, message_dict)
        log_record.pop("color_message", None)
