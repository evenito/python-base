import logging
import sys
from typing import Any

from evenito.logging.json_logger import PinoJsonFormatter


def get_logging_config() -> dict[str, Any]:
    """Get log config object that can be used to setup python logging framework."""
    return {
        "version": 1,
        "disable_existing_loggers": True,
        "formatters": {
            "default":  {"()": "evenito.logging.json_logger.PinoJsonFormatter"},
            "json":  {"()": "evenito.logging.json_logger.PinoJsonFormatter"},
        },
        "handlers": {
            "default": {
                "class": "logging.StreamHandler",
                "formatter": "json",
                "stream": "ext://sys.stdout",
            },
        },
        "root": {
            "handlers": ["default"],
        },
    }


def setup_logger(logger: logging.Logger, log_level: int | str) -> None:
    """Set one logger instance to use structured logging."""
    logger.setLevel(log_level)
    handler = logging.StreamHandler(sys.stdout)
    handler.setLevel(log_level)
    formatter = PinoJsonFormatter()  # type: ignore[no-untyped-call]
    handler.setFormatter(formatter)
    logger.addHandler(handler)


def get_uvicorn_logging_config() -> dict[str, Any] | None:
    """Generate a logging config for uvicorn."""
    try:
        import uvicorn
        import uvicorn.config
    except ModuleNotFoundError:
        return None

    log_config: dict[str, Any] = uvicorn.config.LOGGING_CONFIG
    log_config["formatters"]["access"] = {"()": "evenito.logging.json_logger.UvicornPinoJsonFormatter"}
    log_config["formatters"]["default"] = {"()": "evenito.logging.json_logger.UvicornPinoJsonFormatter"}
    return log_config
