import logging
from base64 import b64encode
from collections.abc import Callable, Coroutine
from typing import Any

from evenito.logging.setup import setup_logger


def create_request_logger() -> Callable[[Any], Coroutine[None, None, None]]:
    """Create a function that can be used a FastAPI route dependency.

    It will log the request in a format that can be consumed by other logging facilities.
    """
    request_logger = logging.getLogger("request")
    setup_logger(request_logger, logging.INFO)

    async def log_request(request: Any) -> None:  # noqa: ANN401  # avoid having to import FastAPI
        payload = {
            "url": request.url,
            "method": request.method,
            "headers": dict(request.headers),
            "params": request.path_params,
            "query": request.query_params,
        }
        if "content-type" in request.headers and request.headers["content-type"].startswith("application/json"):
            payload["body"] = await request.json()
        else:
            payload["body_base64"] = b64encode(await request.body()).decode("utf-8")

        request_logger.info("request", extra=payload)

    return log_request
