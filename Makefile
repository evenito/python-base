.PHONY: install
install:
	poetry install --all-extras

.PHONY: lint
lint:
	poetry run ruff check .

.PHONY: lint-fix
lint-fix:
	poetry run ruff check --fix .

.PHONY: typecheck
typecheck:
	poetry run mypy --strict .

.PHONY: check
check: lint typecheck

.PHONY: test
test:
	docker compose up -d postgres
	poetry run python -m pytest

.PHONY: test-ci
test-ci:
	poetry run python -m pytest
